SRCDIR=src
INCDIR=include
BUILDDIR=build

CC=gcc
CFLAGS=-std=gnu11 -I$(INCDIR)

.PHONY: builddir

all: readrom

readrom: builddir readrom.o cpu.o utils.o memory.o registers.o video.o interrupt.o cb_opcodes.o cpu_opcodes.o
	$(CC) $(CFLAGS) $(BUILDDIR)/readrom.o \
					$(BUILDDIR)/cpu.o $(BUILDDIR)/utils.o \
					$(BUILDDIR)/memory.o $(BUILDDIR)/registers.o \
					$(BUILDDIR)/video.o $(BUILDDIR)/interrupt.o \
					$(BUILDDIR)/cb_opcodes.o $(BUILDDIR)/cpu_opcodes.o \
					-o $(BUILDDIR)/readrom

readrom.o: builddir $(SRCDIR)/readrom.c
	$(CC) $(CFLAGS) -c $(SRCDIR)/readrom.c -o $(BUILDDIR)/readrom.o

cpu.o: builddir $(SRCDIR)/cpu.c
	$(CC) $(CFLAGS) -c $(SRCDIR)/cpu.c -o $(BUILDDIR)/cpu.o

utils.o: builddir $(SRCDIR)/utils.c
	$(CC) $(CFLAGS) -c $(SRCDIR)/utils.c -o $(BUILDDIR)/utils.o
	
memory.o: builddir $(SRCDIR)/memory.c
	$(CC) $(CFLAGS) -c $(SRCDIR)/memory.c -o $(BUILDDIR)/memory.o
	
registers.o: builddir $(SRCDIR)/registers.c
	$(CC) $(CFLAGS) -c $(SRCDIR)/registers.c -o $(BUILDDIR)/registers.o
	
video.o: builddir $(SRCDIR)/video.c
	$(CC) $(CFLAGS) -c $(SRCDIR)/video.c -o $(BUILDDIR)/video.o
	
interrupt.o: builddir $(SRCDIR)/interrupt.c
	$(CC) $(CFLAGS) -c $(SRCDIR)/interrupt.c -o $(BUILDDIR)/interrupt.o

cb_opcodes.o: builddir $(SRCDIR)/cb_opcodes.c
	$(CC) $(CFLAGS) -c $(SRCDIR)/cb_opcodes.c -o $(BUILDDIR)/cb_opcodes.o

cpu_opcodes.o: builddir $(SRCDIR)/cpu_opcodes.c
	$(CC) $(CFLAGS) -c $(SRCDIR)/cpu_opcodes.c -o $(BUILDDIR)/cpu_opcodes.o

builddir: $(BUILDDIR)

$(BUILDDIR):
	mkdir -p $(BUILDDIR)

clean:
	rm -rf $(BUILDDIR)
