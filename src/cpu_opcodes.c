#include <stdio.h>
#include "registers.h"
#include "memory.h"
#include "cpu.h"
#include "cpu_opcodes.h"

extern struct registers registers;


void rotate(uint8_t* param, uint8_t left, uint8_t carry) {
	uint8_t tmp = *param;
	uint8_t tmp_carry = 0;
	
	if (left) {	// RLA/RLCA
		if (carry) {	// RLCA
			registers.carry = !!(tmp & 0x80);
		} else {		// RLA
			tmp_carry = !!(tmp & 0x80);
		}
		
		tmp = tmp << 1;

		if (registers.carry) {
			tmp |= 1;
		} else {
			tmp &= ~1;
		}
	} else {	// RRA/RRCA
		if (carry) {	// RRCA
			registers.carry = (tmp & 1);
		} else {		// RRA
			tmp_carry = (tmp & 1);
		}
		tmp = tmp >> 1;
		
		if (registers.carry) {
			tmp |= (1 << 7);
		} else {
			tmp &= ~(1 << 7);
		}
	}
	
	registers.zero = 0;

	if (!carry) {
		registers.carry = tmp_carry;
	}

	*param = tmp;
}

/**
 * daa - Implements the BCD adjustment DAA opcode
 */
void daa(void) {
	uint8_t scratch = registers.a;

	if (registers.subtract) {	// SUB or SBC
		if (registers.carry) {	// Carry
			if (registers.half_carry) {
				scratch += 0x9A;
			} else {	// No half carry
				scratch += 0xA0;
			}
		} else {	// No carry
			if (registers.half_carry) {
				scratch += 0xFA;
			} else {
				// Do nothing
			}
			registers.carry = 1;
		}
	} else {					// ADD or ADC
		if (registers.carry) {	// Carry
			if (registers.half_carry || ((scratch & 0x0F) > 0x9)) {
				scratch += 0x66;
			} else {	// No half carry
				scratch += 0x60;
			}
		} else {		// No carry
			if (registers.half_carry) {
				if ((scratch & 0xF0) > 0x90) {
					scratch += 0x66;
					registers.carry = 1;
				} else {	// No half carry
					scratch += 0x06;
					registers.carry = 0;
				}
			} else {	// No carry
				if (((scratch & 0xF0) <= 0x90) && ((scratch & 0x0F) <= 0x09)) {
					// Do nothing
					registers.carry = 0;
				} else if (((scratch & 0xF0) <= 0x80) && ((scratch & 0x0F) <= 0x0F)) {
					scratch += 0x06;
					registers.carry = 0;
				} else if (((scratch & 0xF0) >= 0x90) && ((scratch & 0x0F) >= 0x0A)) {
					scratch += 0x66;
					registers.carry = 1;
				} else if (((scratch & 0xF0) >= 0xA0) && ((scratch & 0x0F) <= 0x09)) {
					scratch += 0x60;
					registers.carry = 1;
				}
			}
		}
	}

	registers.a = scratch;
	registers.half_carry = 0;

	if (registers.a) {
		registers.zero = 0;
	} else {
		registers.zero = 1;
	}
}

/**
 * byte_add - Adds two bytes together, setting/clearing the appropriate flags
 * @param dest Pointer to operand storing the result
 * @param value Value to be added to the dest
 */
void byte_add(uint8_t *dest, uint8_t value) {
	uint16_t result = *dest + value;

	// Carry occurred if bit 7 carried into bit 8
	if (result > 0xFF) {
		registers.carry = 1;
	} else {
		registers.carry = 0;
	}

	// Is the result zero?
	if (0 == result) {
		registers.zero = 1;
	} else {
		registers.zero = 0;
	}

	// Did the operation result in a half carry?
	// (bits from low nibble incremented into high nibble)
	if (HALF_CARRY_CHECK_8(*dest, result)) {
		registers.half_carry = 1;
	} else {
		registers.half_carry = 0;
	}
	// Clear the subtract flag
	registers.subtract = 0;

	// Store the result
	*dest = (uint8_t)(result & 0xFF);
}

/**
 * word_add - Adds two words (16 bits) together and sets/clears the appropriate flags
 * @param dest Operand that will hold the result
 * @param value Value to add to dest
 */
void word_add(uint16_t *dest, uint16_t value) {
	uint32_t result = *dest + value;

  // The carry flag is set if result borrows from 15th bit
	if (result > 0xFFFF) {
		registers.carry = 1;
	} else {
		registers.carry = 0;
	}

	if (0 == result) {
		registers.zero = 1;
	} else {
		registers.zero = 0;
	}

  // Half-carry flag is set if result borrows from 11th bit
	if (HALF_CARRY_CHECK_16(*dest, result)) {
		registers.half_carry = 1;
	} else {
		registers.half_carry = 0;
	}

	registers.subtract = 0;

	*dest = (uint16_t)(result & 0xFFFF);
}

/**
 * byte_dec - Decrement a byte's value and set the appropriate flags
 * @param dest Value to decrement
 */
void byte_dec(uint8_t *dest) {
	registers.subtract = 1;
	
	if (0 == ((*dest) - 1)) {
		registers.zero = 1;
	} else {
		registers.zero = 0;
	}

	if (((*dest) & 0x0F) < 1) {
		registers.half_carry = 1;
	} else {
		registers.half_carry = 0;
	}

	*dest = ((*dest) & 0xFF) - 1;
}

/**
 * byte_xor - Performs an XOR of register A with the parameter, storing result
 * in register A
 * @param param Value to XOR against register A
 */
void byte_xor(uint8_t param) {
	registers.a = registers.a ^ (param & 0xFF);
	if (0 == param) {
		registers.zero = 1;
	} else {
		registers.zero = 0;
	}

	registers.carry = 0;
	registers.half_carry = 0;
	registers.subtract = 0;
}

/**
 * byte_or - Performs an OR of register A with the parameter, storing result
 * in register A
 * @param param Value to OR against register A
 */
void byte_or(uint8_t param) {
	registers.a = (registers.a | (param & 0xFF));
	if (0 == registers.a) {
		registers.zero = 1;
	} else {
		registers.zero = 0;
	}

	registers.carry = 0;
	registers.half_carry = 0;
	registers.subtract = 0;
}

/**
 * byte_and - performs a boolean AND against register A
 * @param param Value used in operation
 */
void byte_and(uint8_t param) {
	registers.half_carry = 1;
	registers.subtract = 0;
	registers.carry = 0;

	if ((registers.a & param) == 0) {
		registers.zero = 1;
	} else {
		registers.zero = 0;
	}

	registers.a &= param;
}

/**
 * byte_cp - compares a value against register A and sets
 * flags based on equality/greater than/less than.
 * @param param Value to compare against A
 */
void byte_cp(uint8_t param) {
	if (registers.a == param) {
		registers.zero = 1;
	} else {
		registers.zero = 0;
	}

	registers.subtract = 1;

	if (registers.a < param) {
		registers.carry = 1;
		registers.half_carry = 0;
	} else {
		registers.carry = 0;
		registers.half_carry = 1;
	}
}

/**
 * byte_sub - Subtracts two numbers and stores the result
 * @param dest First value and stores result
 * @param value Amount to subtract from dest
 */
void byte_sub(uint8_t* dest, uint8_t value) {
	registers.subtract = 1;

	if (0 == (*dest - (value & 0xFF))) {
		registers.zero = 1;
	} else {
		registers.zero = 0;
	}

	if (((*dest) & 0x0F) < (value & 0x0F)) {
		registers.half_carry = 1;
	} else {
		registers.half_carry = 0;
	}

  if (*dest < value) {
    registers.carry = 1;
  } else {
    registers.carry = 0;
  }

	*dest = *dest - (value & 0xFF);
}

/**
 * stack_push - Pushes a 16-bit value onto the stack and
 * adjusts SP accordingly
 * @param val Value to be pushed onto the stack
 */
void stack_push(uint16_t val) {
	write_byte_unsigned(--registers.sp, (uint8_t)((val >> 8) & 0xFF));
//	printf("PUSHed 0x%02X to SP 0x%04X (readback: 0x%02X) ", (uint8_t)((val >> 8) & 0xFF), registers.sp, read_byte_unsigned(registers.sp));
	write_byte_unsigned(--registers.sp, (uint8_t)(val & 0xFF));
//	printf("PUSHed 0x%02X to SP 0x%04X (readback: 0x%02X)\r\n", (uint8_t)(val & 0xFF), registers.sp, read_byte_unsigned(registers.sp));
}

/**
 * stack_pop - Pops a 16-bit value off the top of the stack
 * and adjusts SP accordingly
 * @return the value popped off the stack
 */
uint16_t stack_pop(void) {
	uint16_t retval = 0;
	retval = read_byte_unsigned(registers.sp);
//	printf("POPped 0x%02X from SP 0x%04X (retval = 0x%04X), ", read_byte_unsigned(registers.sp), registers.sp, retval);
	retval |= ((uint16_t)read_byte_unsigned(++registers.sp) << 8);
//	printf("POPped 0x%02X from SP 0x%04X (retval = 0x%04X)\r\n", read_byte_unsigned(registers.sp), registers.sp, retval);
	registers.sp++;
	return retval;
}
