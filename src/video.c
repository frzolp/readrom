/*
 * video.c
 *
 *  Created on: Dec 12, 2016
 *      Author: fzolp
 */

#include "memory.h"
#include "interrupt.h"
#include "video.h"

extern uint8_t vram[VRAM_SIZE];
extern uint8_t oam[OAM_SIZE];
extern uint8_t ioram[IO_SIZE];
extern uint8_t interrupt_enable;

struct video_registers* video_registers = (struct video_registers*)&ioram[OFFSET_LCDC - IO_START];
struct interrupt_enable* int_en = (struct interrupt_enable*)&interrupt_enable;
struct interrupt_flag* interrupt_flag = (struct interrupt_flag*)&ioram[OFFSET_IF - IO_START];

static int video_ticks = 0;
extern uint32_t ticks;

void step_lcd(void) {
	static int prev_ticks = 0;

	video_ticks += ticks - prev_ticks;
	prev_ticks = ticks;

	switch (video_registers->mode_flag) {
	case STAT_HBLANK:
		if (video_ticks >= 204) {
			video_registers->ly++;
			if (video_registers->ly == 143) {
				if (int_en->vblank_enable) {
					interrupt_flag->vblank_irq = 1;
				}
				video_registers->mode_flag = STAT_VBLANK;
			} else {
				video_registers->mode_flag = STAT_OAM;
			}
			video_ticks -= 204;
		}
		break;
	case STAT_VBLANK:
		if (video_ticks >= 456) {
			video_registers->ly++;
			if (video_registers->ly > 153) {
				video_registers->ly = 0;
				video_registers->mode_flag = STAT_OAM;
			}
			video_ticks -= 456;
		}
		break;
	case STAT_OAM:
		if (video_ticks >= 80) {
			video_registers->mode_flag = STAT_TRANSFER;
			video_ticks -= 80;
		}
		break;
	case STAT_TRANSFER:
		if (video_ticks >= 172) {
			video_registers->mode_flag = STAT_HBLANK;
			video_ticks -= 172;
		}
		break;
	}
}

void lcd_init(void) {
	video_registers->mode_flag = STAT_HBLANK;
}
