#include "mbc.h"
#include "carts.h"

extern gb_rom romfile;

void mbc1_set_rom_bank(uint8_t bank);
void mbc1_set_ram_bank(uint8_t bank);
void mbc1_enable_ram(void);
void mbc1_disable_ram(void);

void mbc_config(uint16_t addr, uint8_t val) {
	if (romfile.cart_type > 0x00 && romfile.cart_type <= 0x03) {
		/* MBC1 */
		if (addr < 0x2000) {
			/* Enable RAM Bank */
		} else if (addr < 0x4000) {
			/* ROM Bank Select */
		} else if (addr < 0x6000) {
			/* RAM Bank Select/ROM Bank Select MSB */
		} else if (addr < 0x8000) {
			/* Memory Model */
		} else {
			/* Shouldn't hit this */
		}

	} else if (romfile.cart_type > 0x04 && romfile.cart_type <= 0x06) {
		/* MBC2 */
	} else if (romfile.cart_type > 0x0A && romfile.cart_type <= 0x0D) {
		/* MMM01 */
	} else if (romfile.cart_type > 0x0E && romfile.cart_type <= 0x13) {
		/* MBC3 */
	} else if (romfile.cart_type > 0x18 && romfile.cart_type <= 0x1E) {
		/* MBC5 */
	} else {
		/* Undefined */
	}
}

void mbc1_set_rom_bank(uint8_t bank) {

}

void mbc1_set_ram_bank(uint8_t bank) {
}

void mbc1_enable_ram(void) {
}

void mbc1_disable_ram(void) {
}
