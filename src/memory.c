/*
 * memory.c
 *
 *  Created on: Dec 12, 2016
 *      Author: fzolp
 */

#include <stdlib.h>
#include <stdio.h>
#include "memory.h"
#include "registers.h"

#define FG_BOLD_GREEN	"\x1b[32;1m"
#define FG_BLACK		"\x1b[30m"
#define BG_WHITE		"\x1b[47m"
#define COLOR_RESET		"\x1b[0m"

//#define DEBUG

#ifdef DEBUG
#define DEBUG_OUTPUT 	1
#else
#define DEBUG_OUTPUT	0
#endif /* DEBUG */

uint8_t vram[VRAM_SIZE];
uint8_t iram[IRAM_SIZE];
uint8_t oam[OAM_SIZE];
uint8_t hram[HRAM_SIZE];
uint8_t ioram[IO_SIZE];
uint8_t wram[WRAM_SIZE];
uint8_t interrupt_enable;
extern uint8_t *rom;

uint8_t lookup_pointer(uint16_t addr, uint8_t** ptr, uint8_t dbg);

uint8_t read_byte_dbg(uint16_t addr) {
	uint8_t* ptr = NULL;

	if (0 == !!lookup_pointer(addr, &ptr, DEBUG_OUTPUT)) {
		// TODO: Error handling
		printf("\n\tread_byte_unsigned: Invalid address 0x%04X\n", addr);
		return 0;
	} else {
		return *ptr;
	}
}

/**
 * Stores an 8-bit signed value at a given address
 * @param addr Address to write value
 * @param val Value to be written
 */
void write_byte_signed(uint16_t addr, int8_t val) {
	int8_t* ptr = NULL;

	if (0 == lookup_pointer(addr, (uint8_t**)&ptr, DEBUG_OUTPUT)) {
		// TODO: Error handling
		printf("\n\twrite_byte_signed:   Invalid address 0x%04X for value 0x%02X\n", addr, val);
	} else {
		*ptr = val;
	}
}

/**
 * Stores an 8-bit unsigned value at a given address
 * @param addr Address to write value
 * @param val Value to be written
 */
void write_byte_unsigned(uint16_t addr, uint8_t val) {
	uint8_t *ptr = NULL;

	if (0 == lookup_pointer(addr, &ptr, DEBUG_OUTPUT)) {
		// TODO: Error handling
		printf("\n\twrite_byte_unsigned: Invalid address 0x%04X for value 0x%02X\n", addr, val);
	} else {
		*ptr = val;
	}
}

/**
 * Stores a 16-bit signed value at a given address
 * @param addr Address to write value
 * @param val Value to be written
 */
void write_word_signed(uint16_t addr, int16_t val) {
	int16_t* ptr = NULL;

	if (0 == lookup_pointer(addr, (uint8_t**)&ptr, DEBUG_OUTPUT)) {
		// TODO: Error handling
		printf("\n\twrite_word_signed:   Invalid address 0x%04X for value 0x%04X\n", addr, val);
	} else {
		*ptr = val;
	}
}

/**
 * Stores a 16-bit unsigned value at a given address
 * @param addr Address to write value
 * @param val Value to be written
 */
void write_word_unsigned(uint16_t addr, uint16_t val) {
	uint16_t* ptr = NULL;

	if (0 == lookup_pointer(addr, (uint8_t**)&ptr, DEBUG_OUTPUT)) {
		// TODO: Error handling
		printf("\n\twrite_word_unsigned: Invalid address 0x%04X for value 0x%04X\n", addr, val);
	} else {
		*ptr = val;
	}
}

/**
 * Gets an 8-bit unsigned value from the given address
 * @param addr Location of data
 * @return value found at the memory address as 8-bit unsigned int
 */
uint8_t read_byte_unsigned(uint16_t addr) {
	uint8_t* ptr = NULL;

	if (0 == !!lookup_pointer(addr, &ptr, DEBUG_OUTPUT)) {
		// TODO: Error handling
		printf("\n\tread_byte_unsigned: Invalid address 0x%04X\n", addr);
		return 0;
	} else {
		return (*ptr & 0xFF);
	}
}

/**
 * Gets an 8-bit signed value from the given address
 * @param addr Location of data
 * @return value found at the memory address as 8-bit signed int
 */
int8_t read_byte_signed(uint16_t addr) {
	int8_t* ptr = NULL;

	if (0 == !!lookup_pointer(addr, (uint8_t**)&ptr, DEBUG_OUTPUT)) {
		// TODO: Error handling
		printf("\n\tread_byte_signed:   Invalid address 0x%04X\n", addr);
		return 0;
	} else {
		return *ptr;
	}
}

/**
 * Gets a 16-bit unsigned value from the given address
 * @param addr Location of data
 * @return value found at the memory address as 16-bit unsigned int
 */
uint16_t read_word_unsigned(uint16_t addr) {
	uint16_t* ptr = NULL;

	if (0 == !!lookup_pointer(addr, (uint8_t**)&ptr, DEBUG_OUTPUT)) {
		// TODO: Error handling
		printf("\n\tread_word_unsigned: Invalid address 0x%04X\n", addr);
		return 0;
	} else {
		return *ptr;
	}
}

/**
 * Gets a 16-bit signed value from the given address
 * @param addr Location of data
 * @return value found at the memory address as 16-bit signed int
 */
int16_t read_word_signed(uint16_t addr) {
	int16_t* ptr = NULL;

	if (0 == !!lookup_pointer(addr, (uint8_t**)&ptr, DEBUG_OUTPUT)) {
		// TODO: Error handling
		printf("\n\tread_word_signed:   Invalid address 0x%04X\n", addr);
		return 0;
	} else {
		return *ptr;
	}
}

/**
 * memory_init - Configures the memory with expected power-on values
 */
void memory_init(void) {
	write_byte_unsigned(0xFF10, 0x80);
	write_byte_unsigned(0xFF11, 0xBF);
	write_byte_unsigned(0xFF12, 0xF3);
	write_byte_unsigned(0xFF14, 0xBF);
	write_byte_unsigned(0xFF16, 0x3F);
	write_byte_unsigned(0xFF19, 0xBF);
	write_byte_unsigned(0xFF1A, 0x7F);
	write_byte_unsigned(0xFF1B, 0xFF);
	write_byte_unsigned(0xFF1C, 0x9F);
	write_byte_unsigned(0xFF1E, 0xBF);
	write_byte_unsigned(0xFF20, 0xFF);
	write_byte_unsigned(0xFF23, 0xBF);
	write_byte_unsigned(0xFF24, 0x77);
	write_byte_unsigned(0xFF25, 0xF3);
	write_byte_unsigned(0xFF26, 0xF1);
	write_byte_unsigned(0xFF40, 0x91);
	write_byte_unsigned(0xFF47, 0xFC);
	write_byte_unsigned(0xFF48, 0xFF);
	write_byte_unsigned(0xFF49, 0xFF);
}

/**
 * lookup_pointer - Gets a pointer to the correct array
 * @param addr Location in Game Boy memory space (0x0000 - 0xFFFF)
 * @param ptr Pointer (to pointer) to be assigned to memory location
 * @return 1 on success, 0 on error
 */
uint8_t lookup_pointer(uint16_t addr, uint8_t** ptr, uint8_t dbg) {
	uint8_t retval = 1;

	if (dbg) printf("(Requesting offset " FG_BOLD_GREEN "0x%04X" COLOR_RESET " from [" FG_BLACK BG_WHITE, addr);
	if (addr <= ROM_BANK0_END) {				// ROM Bank 0 space
		*ptr = rom + addr;
		if (dbg) printf("ROM BANK 0");
	} else if (addr <= ROM_BANKX_END) {			// Switched ROM Bank space
		/* TODO: Handle switched ROM bank */
		*ptr = rom + addr;
		if (dbg) printf("ROM BANK 1");
	} else if (addr <= VRAM_END) {				// VRAM space
		*ptr = vram + (addr - VRAM_START);
		if (dbg) printf("   VRAM   ");
	} else if (addr <= IRAM_END) {				// Internal RAM space
		*ptr = iram + (addr - IRAM_START);
		if (dbg) printf("   IRAM   ");
	} else if (addr <= WRAM_BANK_END) {			// Switched RAM Bank space
		/* TODO: Handle switched RAM bank */
		*ptr = wram + (addr - WRAM_BANK_START);
		if (dbg) printf("   WRAM   ");
	} else if (addr <= IRAM_ECHO_END) {			// IRAM Echo space
		*ptr = iram + (addr - IRAM_ECHO_START);
		if (dbg) printf(" IRAMECHO ");
	} else if (addr <= OAM_END) {				// OAM space
		*ptr = oam + (addr - OAM_START);
		if (dbg) printf("    OAM   ");
	} else if (addr <= IO_END) {				// IO space
		/* TODO: Handle IO */
		*ptr = ioram + (addr - IO_START);
		if (dbg) printf("    IO    ");
	} else if (addr <= HRAM_END) {				// High RAM space
		*ptr = hram + (addr - HRAM_START);
		if (dbg) printf("   HRAM   ");
	} else if (addr == INTERRUPT_ENABLE) {
		*ptr = &interrupt_enable;
		if (dbg) printf("    IE    ");
	} else {									// Unknown area
		*ptr = NULL;
		retval = 0;
	}
	if (dbg) printf(COLOR_RESET ") \n");
	return retval;
}
