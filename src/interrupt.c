/*
 * interrupt.c
 *
 *  Created on: Dec 14, 2016
 *      Author: fzolp
 */

#include "cpu.h"
#include "registers.h"
#include "memory.h"
#include "interrupt.h"

extern uint8_t interrupt_enable;
extern uint8_t ioram[IO_SIZE];
extern struct registers registers;
extern uint32_t ticks;

uint8_t ime;

struct interrupt_enable* ie = (struct interrupt_enable*)&interrupt_enable;
struct interrupt_flag* int_flag = (struct interrupt_flag*)&ioram[OFFSET_IF - IO_START];

void step_interrupt(void) {
	if ((1 == !!ime) && interrupt_enable && int_flag->int_flag) {
		if (int_flag->vblank_irq && ie->vblank_enable) {
			int_flag->vblank_irq = 0;
			vblank_interrupt();
		}
		if (int_flag->lcd_stat_irq && ie->lcd_stat_enable) {
			int_flag->lcd_stat_irq = 0;
			lcd_stat_interrupt();
		}
		if (int_flag->timer_irq && ie->timer_enable) {
			int_flag->timer_irq = 0;
			timer_interrupt();
		}
		if (int_flag->serial_irq && ie->serial_enable) {
			int_flag->serial_irq = 0;
			serial_interrupt();
		}
		if (int_flag->joypad_irq && ie->joypad_enable) {
			int_flag->joypad_irq = 0;
			joypad_interrupt();
		}
	}
}

void vblank_interrupt(void) {
	// TODO: The drawing operation
	ime = 0;
	stack_push(registers.pc);
	registers.pc = 0x40;
	ticks += 12;
}

void lcd_stat_interrupt(void) {
	ime = 0;
	stack_push(registers.pc);
	registers.pc = 0x48;
	ticks += 12;
}

void timer_interrupt(void) {
	ime = 0;
	stack_push(registers.pc);
	registers.pc = 0x50;
	ticks += 12;
}

void serial_interrupt(void) {
	ime = 0;
	stack_push(registers.pc);
	registers.pc = 0x58;
	ticks += 12;
}

void joypad_interrupt(void) {
	ime = 0;
	stack_push(registers.pc);
	registers.pc = 0x60;
	ticks += 12;
}

void return_interrupt(void) {
	ime = 1;
	registers.pc = stack_pop();
}
