#include <stdio.h>
#include <unistd.h>
#include "registers.h"
#include "cpu.h"
#include "memory.h"
#include "utils.h"
#include "cb_opcodes.h"
#include "cpu_opcodes.h"

#define DEST_REG_INDEX(r)	((r >> 3) & 0x7)
#define SRC_REG_INDEX(r)	(r & 0x7)

#define REGPAIR_INDEX(r)	((r >> 4) & 0x3)
#define DEST_REGPAIR_INDEX(r)	((r >> 2) & 0x3)

#define RST_POS(r)			((r >> 3) & 0x7)
#define RST_ADDR(r)			(r & 0x38)

extern struct registers registers;
extern uint8_t* rom;
extern uint32_t ops;
extern uint8_t ime;

uint32_t ticks;

uint8_t* array_of_regs[] = {
	&registers.b,
	&registers.c,
	&registers.d,
	&registers.e,
	&registers.h,
	&registers.l,
	NULL,
	&registers.a
};

static uint16_t* regpairs[] = {
	&registers.bc,
	&registers.de,
	&registers.hl,
	&registers.sp
};

static uint16_t* stack_regpairs[] = {
	&registers.bc,
	&registers.de,
	&registers.hl,
	&registers.af
};

char* array_of_reg_letters[] = { "B", "C", "D", "E", "H", "L", "!", "A" };

char* regpair_letters[] = { "BC", "DE", "HL", "SP" };
char* stack_regpair_letters[] = { "BC", "DE", "HL", "AF" };

char* msg_format_3_str_str = ANSI_COLOR_RESET "[" ANSI_COLOR_CYAN "0x%04X" ANSI_COLOR_RESET "] "
		ANSI_COLOR_YELLOW "%-4s" ANSI_COLOR_RESET " "
		ANSI_COLOR_GREEN "%-4s" ANSI_COLOR_RESET ","
		ANSI_COLOR_GREEN "%-4s" ANSI_COLOR_RESET "  \n";

char* msg_format_3_int_int = ANSI_COLOR_RESET "[" ANSI_COLOR_CYAN "0x%04X" ANSI_COLOR_RESET "] "
		ANSI_COLOR_YELLOW "%-4s" ANSI_COLOR_RESET " "
		ANSI_COLOR_GREEN "%4d" ANSI_COLOR_RESET ","
		ANSI_COLOR_GREEN "%4d" ANSI_COLOR_RESET "  \n";
char* msg_format_3_int_ahex = ANSI_COLOR_RESET "[" ANSI_COLOR_CYAN "0x%04X" ANSI_COLOR_RESET "] "
		ANSI_COLOR_YELLOW "%-4s" ANSI_COLOR_RESET " "
		ANSI_COLOR_GREEN "%4d" ANSI_COLOR_RESET ","
		ANSI_COLOR_GREEN "(%04X)" ANSI_COLOR_RESET "  \n";
char* msg_format_3_int_str = ANSI_COLOR_RESET "[" ANSI_COLOR_CYAN "0x%04X" ANSI_COLOR_RESET "] "
		ANSI_COLOR_YELLOW "%-4s" ANSI_COLOR_RESET " "
		ANSI_COLOR_GREEN "%4d" ANSI_COLOR_RESET ","
		ANSI_COLOR_GREEN "%-4s" ANSI_COLOR_RESET "  \n";
char* msg_format_3_hex_str = ANSI_COLOR_RESET "[" ANSI_COLOR_CYAN "0x%04X" ANSI_COLOR_RESET "] "
		ANSI_COLOR_YELLOW "%-4s" ANSI_COLOR_RESET " "
		ANSI_COLOR_GREEN "%04X" ANSI_COLOR_RESET ","
		ANSI_COLOR_GREEN "%-4s" ANSI_COLOR_RESET "  \n";
char* msg_format_3_ahex_str = ANSI_COLOR_RESET "[" ANSI_COLOR_CYAN "0x%04X" ANSI_COLOR_RESET "] "
		ANSI_COLOR_YELLOW "%-4s" ANSI_COLOR_RESET " "
		ANSI_COLOR_GREEN "(%04X)" ANSI_COLOR_RESET ","
		ANSI_COLOR_GREEN "%-4s" ANSI_COLOR_RESET "  \n";
char* msg_format_3_str_hex = ANSI_COLOR_RESET "[" ANSI_COLOR_CYAN "0x%04X" ANSI_COLOR_RESET "] "
		ANSI_COLOR_YELLOW "%-4s" ANSI_COLOR_RESET " "
		ANSI_COLOR_GREEN "%-4s" ANSI_COLOR_RESET ","
		ANSI_COLOR_GREEN "%04X" ANSI_COLOR_RESET "  \n";
char* msg_format_3_str_int = ANSI_COLOR_RESET "[" ANSI_COLOR_CYAN "0x%04X" ANSI_COLOR_RESET "] "
		ANSI_COLOR_YELLOW "%-4s" ANSI_COLOR_RESET " "
		ANSI_COLOR_GREEN "%-4s" ANSI_COLOR_RESET ","
		ANSI_COLOR_GREEN "%4d" ANSI_COLOR_RESET "  \n";
char* msg_format_3_str_ahex = ANSI_COLOR_RESET "[" ANSI_COLOR_CYAN "0x%04X" ANSI_COLOR_RESET "] "
		ANSI_COLOR_YELLOW "%-4s" ANSI_COLOR_RESET " "
		ANSI_COLOR_GREEN "%-4s" ANSI_COLOR_RESET ","
		ANSI_COLOR_GREEN "(%04X)" ANSI_COLOR_RESET "  \n";
char* msg_format_2_str = ANSI_COLOR_RESET "[" ANSI_COLOR_CYAN "0x%04X" ANSI_COLOR_RESET "] "
		ANSI_COLOR_YELLOW "%-4s" ANSI_COLOR_RESET " "
		ANSI_COLOR_GREEN "%-4s" ANSI_COLOR_RESET "       \n";
char* msg_format_2_hex = ANSI_COLOR_RESET "[" ANSI_COLOR_CYAN "0x%04X" ANSI_COLOR_RESET "] "
		ANSI_COLOR_YELLOW "%-4s" ANSI_COLOR_RESET " "
		ANSI_COLOR_GREEN "%04X" ANSI_COLOR_RESET "       \n";
char* msg_format_2_ahex = ANSI_COLOR_RESET "[" ANSI_COLOR_CYAN "0x%04X" ANSI_COLOR_RESET "] "
		ANSI_COLOR_YELLOW "%-4s" ANSI_COLOR_RESET " "
		ANSI_COLOR_GREEN "(%04X)" ANSI_COLOR_RESET "       \n";
char* msg_format_1 = ANSI_COLOR_RESET "[" ANSI_COLOR_CYAN "0x%04X" ANSI_COLOR_RESET "] "
		ANSI_COLOR_YELLOW "%-4s" ANSI_COLOR_RESET "            \n";
char* msg_format_err = ANSI_COLOR_RESET "[" ANSI_COLOR_CYAN "0x%04X" ANSI_COLOR_RESET "] "
		ANSI_COLOR_RED "Unknown opcode:" ANSI_COLOR_RESET ANSI_COLOR_RED_BOLD " %02X" ANSI_COLOR_RESET "\n";
char* msg_format_cb_err = ANSI_COLOR_RESET "[" ANSI_COLOR_CYAN "0x%04X" ANSI_COLOR_RESET "] "
		ANSI_COLOR_RED "Unknown" ANSI_COLOR_RESET " " ANSI_COLOR_CYAN_BOLD "CB" ANSI_COLOR_RESET 
		" " ANSI_COLOR_RED"opcode:" ANSI_COLOR_RESET ANSI_COLOR_RED_BOLD " %02X" ANSI_COLOR_RESET "\n";

void daa(void);
void rotate(uint8_t* param, uint8_t left, uint8_t carry);
void byte_add(uint8_t* dest, uint8_t value);
void word_add(uint16_t* dest, uint16_t value);
void byte_sub(uint8_t* dest, uint8_t value);

// Boolean ops
void byte_dec(uint8_t* dest);
void byte_and(uint8_t param);
void byte_xor(uint8_t param);
void byte_or(uint8_t param);
void byte_cp(uint8_t param);

// Stack ops
void stack_push(uint16_t val);
uint16_t stack_pop(void);

/**
 * cpu_init - Sets the registers to their expected startup values. 
 */
void cpu_init(void) {
	ticks = 0;

	registers.a = 0x01;
	registers.f = 0xB0;
	registers.bc = 0x0013;
	registers.de = 0x00D8;
	registers.hl = 0x014D;
	registers.sp = 0xFFFE;
	registers.pc = 0x0100;
}

/**
 * cpu_step - "Executes" opcodes
 */
uint8_t cpu_step(void) {
	// A temp work variable
	uint16_t scratch = 0;
	// Used to indicate current position (since some ops increment PC)
	uint16_t current_pc = registers.pc;
	// Read the instruction at the PC then increment PC
	uint8_t romptr = read_byte_unsigned(registers.pc++);
	enum interrupt_state {
		TOGGLE_IGNORE = 0,
		ENABLE_NEXT = 1,
		ENABLE_NOW = 2,
		DISABLE_NEXT = 3,
		DISABLE_NOW = 4
	} static interrupt_state = TOGGLE_IGNORE;

	ops++;

	switch (romptr & 0xFF) {
		case 0x00:	// NOP
			printf(msg_format_1, current_pc, "NOP");
			ticks += 2;
			break;
		case 0x01:	// LD BC,d16
		case 0x11:	// LD DE,d16
		case 0x21:	// LD HL,d16
		case 0x31:	// LD SP,d16
			scratch = (read_byte_unsigned(registers.pc++) & 0xFF);
			scratch |= (read_byte_unsigned(registers.pc++) & 0xFF) << 8;
			printf(msg_format_3_str_hex, current_pc, "LD", regpair_letters[REGPAIR_INDEX(romptr)], scratch);
			*(regpairs[REGPAIR_INDEX(romptr)]) = scratch;
			ticks += 6;
			break;
		case 0x02:	// LD (BC), A
		case 0x12:	// LD (DE), A
		case 0x22:	// LD (HLI),A
		case 0x32:	// LD (HLD),A
			scratch = read_byte_unsigned(*(regpairs[REGPAIR_INDEX(romptr)]));
			printf(msg_format_3_ahex_str, current_pc, "LD", *(regpairs[REGPAIR_INDEX(romptr)]), "A");
			write_byte_unsigned(*(regpairs[REGPAIR_INDEX(romptr)]), registers.a);
			if (0x22 == romptr) {			// HLI
				registers.hl++;
			} else if (0x32 == romptr) {	// HLD
				registers.hl--;
			}
			ticks += 4;
			break;
		case 0x03:	// INC BC
		case 0x13:	// INC DE
		case 0x23:	// INC HL
		case 0x33:	// INC SP
			printf(msg_format_2_str, current_pc, "INC", regpair_letters[REGPAIR_INDEX(romptr)]);
			/*
			 * Postfix increment/decrement operators take precedence over pointer dereference,
			 * which sneakily breaks how I implemented this instruction unless I wrap the deref
			 * in parentheses.
			 */
			(*(regpairs[REGPAIR_INDEX(romptr)]))++;
			ticks += 4;
			break;
		case 0x04:	// INC B
		case 0x14:	// INC D
		case 0x24:	// INC H
		case 0x0C:	// INC C
		case 0x1C:	// INC E
		case 0x2C:	// INC L
		case 0x3C:	// INC A
			printf(msg_format_2_str, current_pc, "INC", array_of_reg_letters[DEST_REG_INDEX(romptr)]);
			byte_add(array_of_regs[DEST_REG_INDEX(romptr)], 1);
			ticks += 2;
			break;
		case 0x05:	// DEC B
		case 0x15:	// DEC D
		case 0x25:	// DEC H
		case 0x0D:	// DEC C
		case 0x1D:	// DEC E
		case 0x2D:	// DEC L
		case 0x3D:	// DEC A
			printf(msg_format_2_str, current_pc, "DEC", array_of_reg_letters[DEST_REG_INDEX(romptr)]);
			byte_dec(array_of_regs[DEST_REG_INDEX(romptr)]);
			ticks += 2;
			break;
		case 0x06:	// LD B,d8
		case 0x16:	// LD D,d8
		case 0x26:	// LD H,d8
		case 0x0E:	// LD C,d8
		case 0x1E:	// LD E,d8
		case 0x2E:	// LD L,d8
		case 0x3E:	// LD A,d8
			scratch = read_byte_unsigned(registers.pc++);
			printf(msg_format_3_str_hex, current_pc, "LD", array_of_reg_letters[DEST_REG_INDEX(romptr)], scratch);
			*(array_of_regs[DEST_REG_INDEX(romptr)]) = scratch;
			ticks += 4;
			break;
		case 0x07: // RLCA
		case 0x0F: // RRCA
			if (romptr == 0x07) {
				printf(msg_format_1, current_pc, "RLCA");
				rotate(&registers.a, 1, 1);
			} else {
				printf(msg_format_1, current_pc, "RRCA");
				rotate(&registers.a, 0, 1);
			}
			ticks += 2;
			break;
		case 0x08:	// LD a16,SP
			scratch = read_byte_unsigned(registers.pc++); 
			scratch |= ((uint16_t)read_byte_unsigned(registers.pc++) << 8);
			printf(msg_format_3_hex_str, current_pc, "LD", scratch, "SP");
			write_byte_unsigned(scratch, (uint8_t)(registers.sp & 0xFF));
			write_byte_unsigned(++scratch, (uint8_t)((registers.sp >> 8) & 0xFF));
			ticks += 10;
			break;
		case 0x09: case 0x19: case 0x29: case 0x39:	// ADD HL,rr
			printf(msg_format_3_str_str, current_pc, "ADD", "HL", regpair_letters[REGPAIR_INDEX(romptr)]);
			word_add(&registers.hl, *(regpairs[REGPAIR_INDEX(romptr)]));
			ticks += 4;
			break;
		case 0x0A:	// LD A,(BC)
		case 0x1A:	// LD A,(DE)
		case 0x2A:	// LD A,(HLI)
		case 0x3A:	// LD A,(HLD)
			scratch = read_byte_unsigned(*(regpairs[REGPAIR_INDEX(romptr)]));
			printf(msg_format_3_str_ahex, current_pc, "LD", "A", *(regpairs[REGPAIR_INDEX(romptr)]));
			registers.a = scratch;
			if (0x2A == romptr) {			// LD A,(HLI)
				registers.hl++;
			} else if (0x3A == romptr) {	// LD A,(HLD)
				registers.hl--;
			}
			ticks += 4;
			break;
		case 0x0B:	// DEC BC
		case 0x1B:	// DEC DE
		case 0x2B:	// DEC HL
		case 0x3B:	// DEC SP
			printf(msg_format_2_str, current_pc, "DEC", regpair_letters[REGPAIR_INDEX(romptr)]);
			/*
			 * Postfix increment/decrement operators take precedence over pointer dereference,
			 * which sneakily breaks how I implemented this instruction unless I wrap the deref
			 * in parentheses.
			 */
			(*(regpairs[REGPAIR_INDEX(romptr)]))--;
			ticks += 4;
			break;
		case 0x10:
			printf(msg_format_2_str, current_pc, "STOP", "0");
			ticks += 2;
			return 1;
			break;
		case 0x17:	// RLA
		case 0x1F:	// RRA
			if (romptr == 0x17) {
				printf(msg_format_1, current_pc, "RLA");
				rotate(&registers.a, 1, 0);
			} else {
				printf(msg_format_1, current_pc, "RRA");
				rotate(&registers.a, 0, 0);
			}
			// RLA/RRA differ from CB RL/RR in that the Z flag is always cleared.
			registers.zero = 0;
			ticks += 2;
			break;
		case 0x18:	// JR r8
			scratch = read_byte_unsigned(registers.pc++);
			printf(msg_format_2_hex, current_pc, "JR", (uint16_t)(registers.pc + (int8_t)scratch));
			// The byte following JR is a signed value (-127 to +129) to adjust PC by
			registers.pc = (uint16_t)(registers.pc + (int8_t)scratch);
			ticks += 6;
			break;
		case 0x20:	// JR NZ,r8
			scratch = read_byte_unsigned(registers.pc++);
			printf(msg_format_3_str_hex, current_pc, "JR", "NZ", (registers.pc + (int8_t)scratch));
			if (0 == registers.zero) {
				registers.pc += (int8_t)scratch;
				ticks += 6;
			} else {
				ticks += 4;
			}
			break;
		case 0x27:	// DAA
			printf(msg_format_1, current_pc, "DAA");
			daa();
			ticks += 2;
			break;
		case 0x28:	// JR Z,r8
			scratch = read_byte_unsigned(registers.pc++);
			printf(msg_format_3_str_hex, current_pc, "JR", "Z", (registers.pc + (int8_t)scratch));
			if (1 == registers.zero) {
				registers.pc += (int8_t)scratch;
				ticks += 6;
			} else {
				ticks += 4;
			}
			break;
		case 0x2F:	// CPL
			printf(msg_format_1, current_pc, "CPL");
			registers.a = ~registers.a;
			registers.subtract = 1;
			registers.half_carry = 1;
			ticks += 2;
			break;
		case 0x30:	// JR NC,r8
			scratch = read_byte_unsigned(registers.pc++);
			printf(msg_format_3_str_hex, current_pc, "JR", "NC", (registers.pc + (int8_t)scratch));
			if (!registers.carry) {
				registers.pc += (int8_t)scratch;
				ticks += 6;
			} else {
				ticks += 4;
			}
			break;
		case 0x34:	// INC (HL)
			printf(msg_format_2_str, current_pc, "INC", "(HL)");
			scratch = read_byte_unsigned(registers.hl);
			byte_add((uint8_t*)&scratch, 1);
			write_byte_unsigned(registers.hl, scratch);
			ticks += 6;
			break;
		case 0x35:	// DEC (HL)
			printf(msg_format_2_str, current_pc, "DEC", "(HL)");
			scratch = read_byte_unsigned(registers.hl);
			byte_dec((uint8_t*)&scratch);
			write_byte_unsigned(registers.hl, scratch);
			ticks += 6;
			break;
		case 0x36:	// LD (HL),d8
			scratch = read_byte_unsigned(registers.pc++);
			printf(msg_format_3_str_hex, current_pc, "LD", "(HL)", scratch);
			write_byte_unsigned(registers.hl, scratch);
			ticks += 6;
			break;
		case 0x37:	// SCF
			printf(msg_format_1, current_pc, "SCF");
			registers.carry = 1;
			registers.half_carry = 0;
			registers.subtract = 0;
			ticks += 2;
			break;
		case 0x3F:	// CCF
			printf(msg_format_1, current_pc, "CCF");
			registers.carry = !registers.carry;
			registers.half_carry = 0;
			registers.subtract = 0;
			ticks += 2;
			break;
		case 0x40: case 0x41: case 0x42: case 0x43: case 0x44: case 0x45: case 0x47:	// LD B,r
		case 0x48: case 0x49: case 0x4A: case 0x4B: case 0x4C: case 0x4D: case 0x4F:	// LD C,r
		case 0x50: case 0x51: case 0x52: case 0x53: case 0x54: case 0x55: case 0x57:	// LD D,r
		case 0x58: case 0x59: case 0x5A: case 0x5B: case 0x5C: case 0x5D: case 0x5F:	// LD E,r
		case 0x60: case 0x61: case 0x62: case 0x63: case 0x64: case 0x65: case 0x67:	// LD H,r
		case 0x68: case 0x69: case 0x6A: case 0x6B: case 0x6C: case 0x6D: case 0x6F:	// LD L,r
		case 0x78: case 0x79: case 0x7A: case 0x7B: case 0x7C: case 0x7D: case 0x7F:	// LD A,r
			printf(msg_format_3_str_str, current_pc, "LD", 
					array_of_reg_letters[DEST_REG_INDEX(romptr)], 
					array_of_reg_letters[SRC_REG_INDEX(romptr)]);
			// The destination and source registers are in the last six bits of the opcode!
			*(array_of_regs[DEST_REG_INDEX(romptr)]) = *(array_of_regs[SRC_REG_INDEX(romptr)]);
			ticks += 2;
			break;
		case 0x46: case 0x4E:	// LD B,(HL);	LD C,(HL)
		case 0x56: case 0x5E:	// LD D,(HL);	LD E,(HL)
		case 0x66: case 0x6E:	// LD H,(HL);	LD L,(HL)
		case 0x7E:				// LD A,(HL)
			scratch = read_byte_unsigned(registers.hl);
			printf(msg_format_3_str_ahex, current_pc, "LD", array_of_reg_letters[DEST_REG_INDEX(romptr)], scratch);
			*(array_of_regs[DEST_REG_INDEX(romptr)]) = scratch;
			ticks += 4;
			break;
		case 0x70: case 0x71: case 0x72: case 0x73: case 0x74: case 0x75: case 0x77:	// LD (HL),r
			printf(msg_format_3_ahex_str, current_pc, "LD", registers.hl, array_of_reg_letters[SRC_REG_INDEX(romptr)]);
			write_byte_unsigned(registers.hl, *(array_of_regs[SRC_REG_INDEX(romptr)]));
			break;
		case 0x76:
			printf(msg_format_1, current_pc, "HALT");
			ticks += 8;
			return 1;
			break;
		case 0x80: case 0x81: case 0x82: case 0x83: case 0x84: case 0x85: case 0x87:	// ADD A,r
			printf(msg_format_3_str_str, current_pc, "ADD", "A", array_of_reg_letters[SRC_REG_INDEX(romptr)]);
			byte_add(&registers.a, *(array_of_regs[SRC_REG_INDEX(romptr)]));
			ticks += 2;
			break;
		case 0x86:	// ADD A,(HL)
		case 0xC6:	// ADD A,d8
			if (romptr == 0x86) {
				printf(msg_format_3_str_ahex, current_pc, "ADD", "A", registers.hl);
				scratch = read_byte_unsigned(registers.hl);
			} else {
				scratch = read_byte_unsigned(registers.pc++);
				printf(msg_format_3_str_hex, current_pc, "ADD", "A", scratch);
			}
			byte_add(&registers.a, scratch);
			ticks += 4;
			break;
		case 0x88: case 0x89: case 0x8A: case 0x8B: case 0x8C: case 0x8D: case 0x8F:	// ADC A,r
			printf(msg_format_3_str_str, current_pc, "ADC", "A", array_of_reg_letters[SRC_REG_INDEX(romptr)]);
			byte_add(&registers.a, *(array_of_regs[SRC_REG_INDEX(romptr)]) + registers.carry);
			ticks += 2;
			break;
		case 0x8E:	// ADC A,(HL)
		case 0xCE:	// ADC A,d8
			if (romptr == 0x8E) {
				printf(msg_format_3_str_ahex, current_pc, "ADC", "A", registers.hl);
				scratch = read_byte_unsigned(registers.hl);
			} else {
				scratch = read_byte_unsigned(registers.pc++);
				printf(msg_format_3_str_hex, current_pc, "ADC", "A", scratch);
			}
			byte_add(&registers.a, scratch + registers.carry);
			ticks += 4;
			break;
		case 0x90: case 0x91: case 0x92: case 0x93: case 0x94: case 0x95: case 0x97:	// SUB r
			printf(msg_format_2_str, current_pc, "SUB", array_of_reg_letters[SRC_REG_INDEX(romptr)]);
			byte_sub(&registers.a, *(array_of_regs[SRC_REG_INDEX(romptr)]));
			ticks += 2;
			break;
		case 0x96:	// SUB (HL)
		case 0xD6:	// SUB d8
			if (romptr == 0x96) {
				printf(msg_format_2_ahex, current_pc, "SUB", registers.hl);
				scratch = read_byte_unsigned(registers.hl);
			} else {
				scratch = read_byte_unsigned(registers.pc++);
				printf(msg_format_2_hex, current_pc, "SUB", scratch);
			}
			byte_sub(&registers.a, scratch);
			ticks += 4;
			break;
		case 0x98: case 0x99: case 0x9A: case 0x9B: case 0x9C: case 0x9D: case 0x9F:	// SBC r
			printf(msg_format_2_str, current_pc, "SBC", array_of_reg_letters[SRC_REG_INDEX(romptr)]);
			byte_sub(&registers.a, *(array_of_regs[SRC_REG_INDEX(romptr)]) + registers.carry);
			ticks += 2;
			break;
		case 0x9E:	// SBC (HL)
		case 0xDE:	// SBC d8
			if (romptr == 0x9E) {
				printf(msg_format_2_ahex, current_pc, "SBC", registers.hl);
				scratch = read_byte_unsigned(registers.hl);
			} else {
				scratch = read_byte_unsigned(registers.pc++);
				printf(msg_format_2_hex, current_pc, "SBC", scratch);
			}
			byte_sub(&registers.a, scratch + registers.carry);
			ticks += 4;
			break;
		case 0xA0: case 0xA1: case 0xA2: case 0xA3: case 0xA4: case 0xA5: case 0xA7:	// AND r
			printf(msg_format_2_str, current_pc, "AND", array_of_reg_letters[SRC_REG_INDEX(romptr)]);
			byte_and(*(array_of_regs[SRC_REG_INDEX(romptr)]));
			ticks += 2;
			break;
		case 0xA6:	// AND (HL)
		case 0xE6:	// AND d8
			if (romptr == 0xA6) {
				scratch = read_byte_unsigned(registers.hl);
				printf(msg_format_2_ahex, current_pc, "AND", registers.hl);
			} else {
				scratch = read_byte_unsigned(registers.pc++);
				printf(msg_format_2_hex, current_pc, "AND", scratch);
			}
			byte_and((uint8_t)scratch & 0xFF);
			ticks += 4;
			break;
		case 0xA8: case 0xA9: case 0xAA: case 0xAB: case 0xAC: case 0xAD: case 0xAF:	// XOR r
			printf(msg_format_2_str, current_pc, "XOR", array_of_reg_letters[SRC_REG_INDEX(romptr)]);
			byte_xor(*(array_of_regs[SRC_REG_INDEX(romptr)]));
			ticks += 4;
			break;
		case 0xAE:	// XOR (HL)
		case 0xEE:	// XOR d8
			if (romptr == 0xAE) {
				scratch = read_byte_unsigned(registers.hl);
				printf(msg_format_2_ahex, current_pc, "XOR", registers.hl);
			} else {
				scratch = read_byte_unsigned(registers.pc++);
				printf(msg_format_2_hex, current_pc, "XOR", scratch);
			}
			byte_xor(scratch);
			ticks += 8;
			break;
		case 0xB0: case 0xB1: case 0xB2: case 0xB3: case 0xB4: case 0xB5: case 0xB7:	// OR r
			printf(msg_format_2_str, current_pc, "OR", array_of_reg_letters[SRC_REG_INDEX(romptr)]);
			byte_or(*(array_of_regs[SRC_REG_INDEX(romptr)]));
			ticks += 2;
			break;
		case 0xB6:	// OR (HL)
		case 0xF6:	// OR d8
			if (romptr == 0xB6) {
				scratch = read_byte_unsigned(registers.hl);
				printf(msg_format_2_ahex, current_pc, "OR", registers.hl);
			} else {
				scratch = read_byte_unsigned(registers.pc++);
				printf(msg_format_2_hex, current_pc, "OR", scratch);
			}
			byte_or(scratch);
			ticks += 8;
			break;
		case 0xB8: case 0xB9: case 0xBA: case 0xBB: case 0xBC: case 0xBD: case 0xBF:	// CP r
			printf(msg_format_2_str, current_pc, "CP", array_of_reg_letters[SRC_REG_INDEX(romptr)]);
			byte_cp(*(array_of_regs[SRC_REG_INDEX(romptr)]));
			ticks += 2;
			break;
		case 0xBE:	// CP (HL)
		case 0xFE:	// CP d8
			if (romptr == 0xBE) {
				scratch = read_byte_unsigned(registers.hl);
				printf(msg_format_2_ahex, current_pc, "CP", registers.hl);
			} else {
				scratch = read_byte_unsigned(registers.pc++);
				printf(msg_format_2_hex, current_pc, "CP", scratch);
			}
			byte_cp(scratch);
			ticks += 4;
			break;
		case 0xC0:	// RET NZ
			printf(msg_format_2_str, current_pc, "RET", "NZ");
			if (!registers.zero) {
				registers.pc = stack_pop();
				ticks += 10;
			} else {
				ticks += 4;
			}
			break;
		case 0xC1:	// POP BC
		case 0xD1:	// POP DE
		case 0xE1:	// POP HL
		case 0xF1:	// POP AF
			printf(msg_format_2_str, current_pc, "POP", stack_regpair_letters[REGPAIR_INDEX(romptr)]);
			*(stack_regpairs[REGPAIR_INDEX(romptr)]) = stack_pop();
			ticks += 6;
			break;
		case 0xC2:	// JP NZ,a16
			scratch = ((read_byte_unsigned(registers.pc++) & 0xFF) | ((read_byte_unsigned(registers.pc++) & 0xFF) << 8));
			printf(msg_format_3_str_hex, current_pc, "JP", "NZ", scratch);
			if (!registers.zero) {
				registers.pc = scratch;
				ticks += 8;
			} else {
				ticks += 6;
			}
			break;
		case 0xC3:	// JP a16
			scratch = read_byte_unsigned(registers.pc++); 
			scratch |= ((uint16_t)read_byte_unsigned(registers.pc++) << 8);
			registers.pc = scratch;
			printf(msg_format_2_hex, current_pc, "JP", scratch);
			ticks += 8;
			break;
		case 0xC4:	// CALL NZ,a16
			scratch = read_byte_unsigned(registers.pc++); 
			scratch |= ((uint16_t)read_byte_unsigned(registers.pc++) << 8);
			printf(msg_format_3_str_hex, current_pc, "CALL", "NZ", scratch);
			if (!registers.zero) {
				stack_push(registers.pc);
				registers.pc = scratch;
				ticks += 8;
			} else {
				ticks += 6;
			}
			break;
		case 0xC5:	// PUSH BC
		case 0xD5:	// PUSH DE
		case 0xE5:	// PUSH HL
		case 0xF5:	// PUSH AF
			printf(msg_format_2_str, current_pc, "PUSH", stack_regpair_letters[REGPAIR_INDEX(romptr)]);
			stack_push(*(stack_regpairs[REGPAIR_INDEX(romptr)]));
			ticks += 8;
			break;
		case 0xC7: case 0xCF:	// RST 00H;	RST 08H
		case 0xD7: case 0xDF:	// RST 10H;	RST 18H
		case 0xE7: case 0xEF:	// RST 20H;	RST 28H
		case 0xF7: case 0xFF:	// RST 30H;	RST 38H
			printf(msg_format_2_hex, current_pc, "RST", RST_ADDR(romptr));
			stack_push(registers.pc++);
			registers.pc = RST_ADDR(romptr);
			ticks += 8;
			break;
		case 0xC8:	// RET Z
			printf(msg_format_2_str, current_pc, "RET", "Z");
			if (registers.zero) {
				registers.pc = stack_pop();
				ticks += 10;
			} else {
				ticks += 4;
			}
			break;
		case 0xC9:	// RET
			printf(msg_format_1, current_pc, "RET");
			registers.pc = stack_pop();
			ticks += 8;
			break;
		case 0xCA:	// JP Z,a16
			scratch = read_byte_unsigned(registers.pc++); 
			scratch |= ((uint16_t)read_byte_unsigned(registers.pc++) << 8);
			printf(msg_format_3_str_hex, current_pc, "JP", "Z", scratch);
			if (registers.zero) {
				registers.pc = scratch;
				ticks += 8;
			} else {
				ticks += 6;
			}
			break;
		case 0xCB:	// PREFIX CB
			scratch = read_byte_unsigned(registers.pc++); 
			if (cb_prefix(romptr, scratch)) {
				return 1;
			}
			ticks += 2;
			break;
		case 0xCC:	// CALL Z,a16
			scratch = read_byte_unsigned(registers.pc++); 
			scratch |= ((uint16_t)read_byte_unsigned(registers.pc++) << 8);
			printf(msg_format_3_str_hex, current_pc, "CALL", "Z", scratch);
			if (registers.zero) {
				stack_push(registers.pc);
				registers.pc = scratch;
				ticks += 8;
			} else {
				ticks += 6;
			}
			break;
		case 0xCD:	// CALL a16
			scratch = read_byte_unsigned(registers.pc++); 
			scratch |= ((uint16_t)read_byte_unsigned(registers.pc++) << 8);
			printf(msg_format_2_hex, current_pc, "CALL", scratch);
			stack_push(registers.pc);
			registers.pc = scratch;
			ticks += 12;
			break;
		case 0xD0:	// RET NC
			printf(msg_format_2_str, current_pc, "RET", "NC");
			if (!registers.carry) {
				registers.pc = stack_pop();
				ticks += 10;
			} else {
				ticks += 4;
			}
			break;
		case 0xD4:	// CALL NC,a16
			scratch = read_byte_unsigned(registers.pc++); 
			scratch |= ((uint16_t)read_byte_unsigned(registers.pc++) << 8);
			printf(msg_format_3_str_hex, current_pc, "CALL", "NC", scratch);
			if (!registers.carry) {
				stack_push(registers.pc++);
				registers.pc = scratch;
				ticks += 8;
			} else {
				ticks += 6;
			}
			break;
		case 0xD9:	// RETI
			printf(msg_format_1, current_pc, "RETI");
			registers.pc = stack_pop();
			interrupt_state = ENABLE_NOW;
			ticks += 8;
			break;
		case 0xE0:	// LDH (a8),A
			scratch = (uint16_t)read_byte_unsigned(registers.pc++) + 0xFF00;
			printf(msg_format_3_hex_str, current_pc, "LD", scratch, "A");
			write_byte_unsigned(scratch, registers.a);
			ticks += 6;
			break;
		case 0xE2:	// LD (C),A
			printf(msg_format_3_ahex_str, current_pc, "LD", 0xFF00 + registers.c, "A");
			write_byte_unsigned(0xFF00 + registers.c, registers.a);
			ticks+=4;
			break;
		case 0xE8:	// ADD SP,r8
			scratch = read_byte_unsigned(registers.pc++);
			printf(msg_format_3_str_hex, current_pc, "ADD", "SP", (int8_t)scratch);
			if ((int8_t)scratch >= 0) {
				registers.carry = (((uint32_t)registers.sp + (int8_t)scratch) > 0xFFFF) ? 1 : 0;
				registers.half_carry = (((registers.sp & 0x0FFF) + (int8_t)scratch) > 0x0FFF) ? 1 : 0;
			} else {
				registers.carry = (registers.sp < ((int8_t)scratch * -1)) ? 1 : 0;
				registers.half_carry = (((int8_t)scratch * -1) > (registers.sp & 0x0FFF)) ? 1 : 0;
			}

			registers.sp += (int8_t)scratch;
			registers.zero = 0;
			registers.subtract = 0;
			ticks += 8;
			break;
		case 0xE9:	// JP (HL)
			printf(msg_format_2_str, current_pc, "JP", "HL");
			registers.pc = registers.hl;
			ticks += 2;
			break;
		case 0xEA:	// LD (a16),A
			scratch = read_byte_unsigned(registers.pc++); 
			scratch |= ((uint16_t)read_byte_unsigned(registers.pc++) << 8);
			printf(msg_format_3_hex_str, current_pc, "LD", scratch, "A");
			write_byte_unsigned(scratch, registers.a);
			ticks += 8;
			break;
		case 0xF0:	// LDH A,(a8)
			scratch = (uint16_t)read_byte_unsigned(registers.pc++) + 0xFF00;
			printf(msg_format_3_str_hex, current_pc, "LDH", "A", scratch);
			registers.a = read_byte_unsigned(scratch);
			ticks += 6;
			break;
		case 0xF2:	// LD A,(C)
			printf(msg_format_3_str_ahex, current_pc, "LD", "A", 0xFF00 + registers.c);
			scratch = read_byte_unsigned(registers.c + 0xFF00);
			registers.a = scratch;
			ticks+=4;
			break;
		case 0xF3:	// DI
			printf(msg_format_1, current_pc, "DI");
			interrupt_state = DISABLE_NEXT;
			ticks += 2;
			break;
		case 0xF8:	// LD HL,SP+r8
			scratch = read_byte_unsigned(registers.pc++);
			printf(msg_format_3_str_hex, current_pc, "LD", "HL", registers.sp + (int8_t)scratch);
			registers.zero = 0;
			registers.subtract = 0;
			if ((int8_t)scratch >= 0) {
				registers.carry = (((uint32_t)registers.sp + (int8_t)scratch) > 0xFFFF) ? 1 : 0;
				registers.half_carry = (((registers.sp & 0x0FFF) + (int8_t)scratch) > 0x0FFF) ? 1 : 0;
			} else {
				registers.carry = (registers.sp < ((int8_t)scratch * -1)) ? 1 : 0;
				registers.half_carry = (((int8_t)scratch * -1) > (registers.sp & 0x0FFF)) ? 1 : 0;
			}
			
			registers.hl = (registers.sp + (int8_t)scratch);
			ticks += 6;
			break;
		case 0xF9:	// LD SP,HL
			printf(msg_format_3_str_str, current_pc, "LD", "SP", "HL");
			registers.sp = registers.hl;
			ticks += 4;
			break;
		case 0xFA:	// LD A,(a16)
			scratch = read_byte_unsigned(registers.pc++); 
			scratch |= ((uint16_t)read_byte_unsigned(registers.pc++) << 8);
			printf(msg_format_3_str_hex, current_pc, "LD", "A", scratch);
			registers.a = read_byte_unsigned(scratch) & 0xFF;
			ticks += 8;
			break;
		case 0xFB:	// EI
			printf(msg_format_1, current_pc, "EI");
			interrupt_state = ENABLE_NEXT;
			ticks += 2;
			break;
		default:
			printf(msg_format_err, current_pc, (romptr & 0xFF));
			return 1;
	}

	print_regs();

	switch (interrupt_state) {
	case ENABLE_NEXT:
		interrupt_state = ENABLE_NOW;
		break;
	case ENABLE_NOW:
		ime = 1;
		interrupt_state = TOGGLE_IGNORE;
		break;
	case DISABLE_NEXT:
		interrupt_state = DISABLE_NOW;
		break;
	case DISABLE_NOW:
		ime = 0;
		interrupt_state = TOGGLE_IGNORE;
		break;
	default:
		break;
	}
	return 0;
}

