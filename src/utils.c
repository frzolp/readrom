#include <stdio.h>
#include <string.h>
#include "registers.h"
#include "carts.h"
#include "memory.h"
#include "video.h"

extern char roms[54][28];
extern struct registers registers;
extern uint8_t *rom;
extern gb_rom romfile;
extern char *cartridges[];
extern char *rams[];
extern uint8_t ram[0xFFFF + 1];
extern uint8_t ioram[((0xFF7F - 0xFF00) + 1)];

const char nintendo[] = {
	// 0                       4                       8                      12
	0xCE, 0xED, 0x66, 0x66, 0xCC, 0x0D, 0x00, 0x0B, 0x03, 0x73, 0x00, 0x83, 0x00, 0x0C, 0x00, 0x0D,
	//16                      20                      24                      28
	0x00, 0x08, 0x11, 0x1F, 0x88, 0x89, 0x00, 0x0E, 0xDC, 0xCC, 0x6E, 0xE6, 0xDD, 0xDD, 0xD9, 0x99,
	//32                      36                      40                      44
	0xBB, 0xBB, 0x67, 0x63, 0x6E, 0x0E, 0xEC, 0xCC, 0xDD, 0xDC, 0x99, 0x9F, 0xBB, 0xB9, 0x33, 0x3E
};

void populate_romtypes(void) {
	strcpy(roms[0], "256kbit = 32kB = 2 banks");
	strcpy(roms[1], "512kbit = 64kB = 4 banks");
	strcpy(roms[2], "1Mbit = 128kB = 8 banks");
	strcpy(roms[3], "2Mbit = 256kB = 16 banks");
	strcpy(roms[4], "4Mbit = 512kB = 32 banks");
	strcpy(roms[5], "8Mbit = 1MB = 64 banks");
	strcpy(roms[6], "16Mbit = 2MB = 128 banks");
}

/**
 * print_logo: Displays the "Nintendo" logo on the console
 *
 * The logo is 48 bytes long, with each row 48 BITS wide.
 *
 * The first row is comprised of the high nibble of the first
 * 12 even bytes, while the second row is made up of the low nibble.
 * The third row is the high nibble of the first 12 odd bytes, with
 * the fourth row is the low nibble. The fifth row is the high nibble
 * of the last 12 even bytes, and so on.
 *
 * Row 1:  0  2  4  6  8 10 12 14 16 18 20 22
 * Row 2:  0  2  4  6  8 10 12 14 16 18 20 22
 * Row 3:  1  3  5  7  9 11 13 15 17 19 21 23
 * Row 4:  1  3  5  7  9 11 13 15 17 19 21 23
 * Row 5: 24 26 28 30 32 34 36 38 40 42 44 46
 * Row 6: 24 26 28 30 32 34 36 38 40 42 44 46
 * Row 7: 25 27 29 31 33 35 37 39 41 43 45 47
 * Row 8: 25 27 29 31 33 35 37 39 41 43 45 47
 */
void print_logo(void) {
	for (int x = 0; x < 2; x++) {
		for (int z = 0; z < 12; z++) {
			for (int y = 7; y >= 4; y--) {
				if ((romfile.nintendo[(z * 2) + x] >> y) & 1) {
					printf("#");
				} else {
					printf(" ");
				}
			}
		}
		printf("\n");
		for (int z = 0; z < 12; z++) {
			for (int y = 3; y >= 0; y--) {
				if ((romfile.nintendo[(z * 2) + x] >> y) & 1) {
					printf("#");
				} else {
					printf(" ");
				}
			}
		}
		printf("\n");
	}

	for (int x = 0; x < 2; x++) {
		for (int z = 0; z < 12; z++) {
			for (int y = 7; y >= 4; y--) {
				if ((romfile.nintendo[24 + (z * 2) + x] >> y) & 1) {
					printf("#");
				} else {
					printf(" ");
				}
			}
		}
		printf("\n");
		for (int z = 0; z < 12; z++) {
			for (int y = 3; y >= 0; y--) {
				if ((romfile.nintendo[24 + (z * 2) + x] >> y) & 1) {
					printf("#");
				} else {
					printf(" ");
				}
			}
		}
		printf("\n");
	}
}

int nintendo_verify(void) {
	printf("Nintendo Logo Verification: ");
	for (int x = 0; x < 0x30; x++) {
		if ((romfile.nintendo[x] & 0xFF) != (nintendo[x] & 0xFF)) {
			printf("Mismatch at offset %02d: ROM = %02X, Expected = %02X\n", x, (romfile.nintendo[x] & 0xFF), (nintendo[x] & 0xFF));
			return 1;
		}
	}
	printf("Passed\n");
	return 0;
}

void cart_details(void) {
	printf("Title: ");
	for (int x = 0; x < 15; x++) {
		printf("%c", (char)romfile.title[x]);
	}
	printf("\n");
	printf("Color: %s\n", ((0x80 == romfile.color) ? "true" : "false"));

	printf("Licensee Code: %02X %02X\n", romfile.hi_license, romfile.lo_license);
	printf("Super Game Boy: %s\n", ((0x03 == romfile.super_gb) ? "true" : "false"));
	printf("Cartridge type: %s\n", cartridges[romfile.cart_type]);
	printf("Cartridge ROM: %s\n", roms[romfile.rom_size]);
	printf("Cartridge RAM: %s\n", rams[romfile.ram_size]);
	printf("Destination: %s\n", ((1 == romfile.destination) ? "Non-Japanese" : "Japanese"));
	printf("Licensee Code (old): %s\n", ((0x33 == romfile.licensee_code) ? "Check new licensee code" : 
										((0x79 == romfile.licensee_code) ? "Accolade" : 
										 				((0xA4 == romfile.licensee_code) ? "Konami" : "Unknown"))));
	printf("Mask ROM version: %02X\n", romfile.mask_rom_version);
	printf("Complement check: %02X\n", romfile.complement_check);
	printf("Checksum: %02X %02X\n", (romfile.checksum[0] & 0xFF), (romfile.checksum[1] & 0xFF));
}

void print_regs(void) {
	printf("(AF:$%04X BC:$%04X DE:$%04X HL:$%04X)", (registers.af & 0xFFFF), (registers.bc & 0xFFFF), (registers.de & 0xFFFF), (registers.hl & 0xFFFF));
	printf("(SP:$%04X PC:$%04X)", (registers.sp & 0xFFFF), (registers.pc & 0xFFFF));
}

void dump_ram(void) {
	printf("      0  1  2  3  4  5  6  7  8  9  A  B  C  D  E  F\n");
	for (int x = 0xFF00; x < 0xFF80; x+= 0x10) {
		printf("%04X: ", x);
		for (int y = x; y < (x + 0x10); y++) {
			printf("%02X ", (read_byte_dbg(y) & 0xFF));
		}
		printf("\n");
	}
}

void print_io_registers(void) {
	struct video_registers* vid_reg = (struct video_registers*)&ioram[0x40];
	printf("Video Registers\n");
	printf("\tLCDC:\n");
	printf("\t\tbg_display:            %d\n", vid_reg->bg_display);
	printf("\t\tobj_display_enable:    %d\n", vid_reg->obj_display_enable);
	printf("\t\tobj_size:              %d\n", vid_reg->obj_size);
	printf("\t\tbg_display_select:     %d\n", vid_reg->bg_display_select);
	printf("\t\tbg_window_data_select: %d\n", vid_reg->bg_window_data_select);
	printf("\t\twindow_display_enable: %d\n", vid_reg->window_display_enable);
	printf("\t\twindow_display_select: %d\n", vid_reg->window_display_select);
	printf("\t\tlcd_display_enable:    %d\n", vid_reg->lcd_display_enable);
	printf("\n");
	printf("\tSTAT:\n");
	printf("\t\tmode_flag:        %d\n", vid_reg->mode_flag);
	printf("\t\tcoincidence_flag: %d\n", vid_reg->coincidence_flag);
	printf("\t\thblank_interrupt: %d\n", vid_reg->hblank_interrupt);
	printf("\t\tvblank_interrupt: %d\n", vid_reg->vblank_interrupt);
	printf("\t\toam_interrupt:    %d\n", vid_reg->oam_interrupt);
	printf("\t\tlyc_interrupt:    %d\n", vid_reg->lyc_interrupt);
	printf("\n");
	printf("\tSCY: %d\n", vid_reg->scy);
	printf("\tSCX: %d\n", vid_reg->scx);
	printf("\tLY:  %d\n", vid_reg->ly);
	printf("\tLYC: %d\n", vid_reg->lyc);
	printf("\tDMA: %d\n", vid_reg->dma);
	printf("\n");
	printf("\tBGP:\n");
	printf("\t\tcolor_0: %d\n", vid_reg->color_0);
	printf("\t\tcolor_1: %d\n", vid_reg->color_1);
	printf("\t\tcolor_2: %d\n", vid_reg->color_2);
	printf("\t\tcolor_3: %d\n", vid_reg->color_3);
	printf("\n");
	printf("\tOBP0: %d\n", vid_reg->obp_0);
	printf("\tOBP1: %d\n", vid_reg->obp_1);
	printf("\tWY:   %d\n", vid_reg->wy);
	printf("\tWX:   %d\n", vid_reg->wx);

}
