#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

int main(int argc, char **argv) {
	FILE *infile, *outfile;
	char* outname;
	int byte_read;

	if (2 != argc) {
		printf("Usage: %s <rom.gb>\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	if ((infile = fopen(argv[1], "r")) == NULL) {
		printf("Failed to open input file: %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}

	outname = calloc(sizeof(char), strlen(outname) + 3);
	strcpy(outname, strtok(argv[1], ".gb"));
	strcat(outname, ".h");

	if ((outfile = fopen(outname, "w")) == NULL) {
		printf("Failed to open output file: %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}

	fprintf(outfile, "#ifndef __ROMFILE_H__\n");
	fprintf(outfile, "#define __ROMFILE_H__\n");

	fprintf(outfile, "const char romfile[] = {\n\t");

	int linewidth = 0;
	while ((byte_read = fgetc(infile)) != EOF) {
		fprintf(outfile, "0x%02X, ", (uint8_t)(byte_read & 0xFF));
		if (15 == linewidth) {
			fprintf(outfile, "\n\t");
			linewidth = 0;
		} else {
			linewidth++;
		}
	}


	fprintf(outfile, "\n};\n");
	fprintf(outfile, "#endif /* __ROMFILE_H__ */");

	fclose(outfile);
	fclose(infile);

	return 0;
}
