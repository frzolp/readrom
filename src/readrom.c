#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <string.h>
#include <unistd.h>
#include "registers.h"
#include "carts.h"
#include "utils.h"
#include "cpu.h"
#include "memory.h"
#include "video.h"
#include "interrupt.h"

/*
 * Function declarations
 */

// Debug output function
uint8_t read_byte_dbg(uint16_t);

/*
 * Global variable definitions
 */

// The CPU itself
struct registers registers;
// A Game Boy ROM
gb_rom romfile;

// Cartridge type descriptions that correspond to the integer values
// found in the ROM header
char *cartridges[] = {
	"ROM ONLY",                 // Only supported type for now
	"ROM+MBC1",
	"ROM+MBC1+RAM",
	"ROM+MBC1+RAM+BATT",
	"UNDEFINED 0x04",
	"ROM+MBC2",
	"ROM+MBC2+BATTERY",
	"UNDEFINED 0x07",
	"ROM+RAM",
	"ROM+RAM+BATTERY",
	"UNDEFINED 0x0A",
	"ROM+MMM01",
	"ROM+MMM01+SRAM",
	"ROM+MMM01+SRAM+BATT",
	"UNDEFINED 0x0E",
	"ROM+MBC3+TIMER+BATT",
	"ROM+MBC3+TIMER+RAM+BATT",
	"ROM+MBC3",
	"ROM+MBC3+RAM",
	"ROM+MBC3+RAM+BATT",
	"UNDEFINED 0x14",
	"UNDEFINED 0x15",
	"UNDEFINED 0x16",
	"UNDEFINED 0x17",
	"UNDEFINED 0x18",
	"ROM+MBC5",
	"ROM+MBC5+RAM",
	"ROM+MBC5+RAM+BATT",
	"ROM+MBC5+RUMBLE",
	"ROM+MBC5+RUMBLE+SRAM",
	"ROM+MBC5+RUMBLE+SRAM+BATT",
	"Pocket Camera"
};

// Cartridge RAM type descriptions corresponding to the
// integer values found in the header
char *rams[] = {
	"None",
	"16kBit = 2kB = 1 bank",
	"64kBit = 8kB = 1 bank",
	"256kBit = 32kB = 4 banks",
	"1MBit = 128kB = 16 banks"
};

// ROM description string
char roms[54][28];
// Unsigned byte pointer to traverse ROM file
uint8_t *rom;
// Game Boy RAM array
uint8_t ram[0xFFFF + 1];
// Crude infinite loop detection limit
int iffy_loops = 3;
// Number of operations
uint32_t ops;

int main(int argc, char **argv) {
	// ROM file pointer
	FILE *fp;

	// Set the number of iterations a loop should run
	// before we freak out and abort
	if (3 == argc) {
		iffy_loops = atoi(argv[2]);
	}

	// Open the ROM file
	if (2 <= argc) {
		fp = fopen(argv[1], "r");
	} else {
		fp = fopen("pokemon.gb", "r");
	}

	if (NULL == fp) {
		printf("Failed to open file\n");
		exit(EXIT_FAILURE);
	}

	// TODO: I think I can do better.
	populate_romtypes();

	// Read the ROM file's header (for ROM size, type, etc)
	fread(&romfile, sizeof(romfile), 1, fp);
	// Go back to the start of the ROM file
	rewind(fp);
	// Allocate memory to store the entirety of the ROM
	rom = calloc(sizeof(uint8_t), (16384 * (2 << romfile.rom_size)));
	// Read the ROM into the allocated memory
	fread(rom, sizeof(uint8_t), (16384 * (2 << romfile.rom_size)), fp);
	// Zero the RAM
	memset(ram, 0, 0xFFFF + 1);

	// See if the ROM has a valid Nintendo logo and exit if not
	if (0 != nintendo_verify()) {
		fclose(fp);
		exit(EXIT_FAILURE);
	}

	ops = 0;
	cart_details();
	cpu_init();
	memory_init();
	lcd_init();
	parse_rom();

	print_io_registers();

	printf("\nTotal ops executed: %d\n", ops);

//	print_logo();

	fclose(fp);
	
	fflush(stdout);

	return 0;
}

void parse_rom(void) {
	uint8_t ouch = 0;
	uint8_t romptr;
	uint8_t iterctr = 0;
	uint16_t last_af = 0, last_bc = 0, last_de = 0;

	while (!ouch) {
		// romptr = rom + registers.pc;
		// romptr = read_byte_dbg(registers.pc);
		//ouch = cpu_step(romptr);
		ouch = cpu_step();
		step_lcd();
		step_interrupt();
		//usleep(75 * 1000);
		if ((last_af == registers.af) && (last_bc == registers.bc) && (last_de == registers.de)
		  && (registers.a == 0) && (registers.bc == 0) && (registers.d == 0) && (romptr == 0x20)) {
			iterctr++;
			printf(" --- POSSIBLE INFINITE LOOP CONDITION #%d ---", iterctr);
			if (iffy_loops == iterctr) {
				printf("\nInfinite loop error!\n");
				dump_ram();
//				print_io_registers();
				break;
			}
		} 
		last_af = registers.af;
		last_bc = registers.bc;
		last_de = registers.de;
		printf("\n");
	}
}
