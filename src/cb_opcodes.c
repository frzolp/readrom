#include <stdio.h>
#include "registers.h"
#include "cpu.h"
#include "memory.h"
#include "cb_opcodes.h"
#include "utils.h"

#define CB_BIT_POS(r)		((r >> 3) & 0x7)
#define SRC_REG_INDEX(r)	(r & 0x7)

extern struct registers registers;
extern uint8_t* array_of_regs[];
extern char* array_of_reg_letters[];

extern uint32_t ticks;
extern char* msg_format_3_str_str;
extern char* msg_format_cb_err;
extern char* msg_format_3_str_ahex;
extern char* msg_format_3_str_int;
extern char* msg_format_3_str_hex;
extern char* msg_format_3_int_ahex;
extern char* msg_format_3_int_str;
extern char* msg_format_2_hex;
extern char* msg_format_2_ahex;
extern char* msg_format_2_str;

void cb_bit(uint8_t bit, uint8_t val);
void cb_swap(uint8_t* param);
void cb_set(uint8_t* param, uint8_t bit);
void cb_sla_srl(uint8_t* param, uint8_t left);
void cb_rotate_carry(uint8_t* param, uint8_t left);
void cb_rotate(uint8_t* param, uint8_t left);
void cb_sra(uint8_t* param);
void cb_res(uint8_t* param, uint8_t bit);

uint8_t cb_prefix(uint8_t pc, uint8_t opcode) {
	uint8_t tmp, bitpos;
	switch (opcode) {
		case 0x00: case 0x01: case 0x02: case 0x03: case 0x04: case 0x05: case 0x07:	// RLC r
		case 0x08: case 0x09: case 0x0A: case 0x0B: case 0x0C: case 0x0D: case 0x0F:	// RRC r
			if (opcode >= 0x08) {
				printf(msg_format_2_str, pc, "RLC", array_of_reg_letters[SRC_REG_INDEX(opcode)]);
				cb_rotate_carry(array_of_regs[SRC_REG_INDEX(opcode)], 1);
			} else {
				printf(msg_format_2_str, pc, "RRC", array_of_reg_letters[SRC_REG_INDEX(opcode)]);
				cb_rotate_carry(array_of_regs[SRC_REG_INDEX(opcode)], 0);
			}
			ticks += 4;
			break;
		case 0x06:	// RLC (HL)
		case 0x0E:	// RRC (HL)
			tmp = read_byte_unsigned(registers.hl);
			if (opcode == 0x06) {
				printf(msg_format_2_ahex, pc, "RLC", registers.hl);
				cb_rotate_carry(&tmp, 1);
			} else {
				printf(msg_format_2_ahex, pc, "RRC", registers.hl);
				cb_rotate_carry(&tmp, 0);
			}
			write_byte_unsigned(registers.hl, tmp);
			ticks += 8;
			break;
		case 0x10: case 0x11: case 0x12: case 0x13: case 0x14: case 0x15: case 0x17:	// RL r
		case 0x18: case 0x19: case 0x1A: case 0x1B: case 0x1C: case 0x1D: case 0x1F:	// RR r
			if (opcode >= 0x08) {
				printf(msg_format_2_str, pc, "RL", array_of_reg_letters[SRC_REG_INDEX(opcode)]);
				cb_rotate(array_of_regs[SRC_REG_INDEX(opcode)], 1);
			} else {
				printf(msg_format_2_str, pc, "RR", array_of_reg_letters[SRC_REG_INDEX(opcode)]);
				cb_rotate(array_of_regs[SRC_REG_INDEX(opcode)], 0);
			}
			ticks += 4;
			break;
		case 0x16:	// RL (HL)
		case 0x1E:	// RR (HL)
			tmp = read_byte_unsigned(registers.hl);
			if (opcode == 0x16) {
				printf(msg_format_2_ahex, pc, "RL", registers.hl);
				cb_rotate(&tmp, 1);
			} else {
				printf(msg_format_2_ahex, pc, "RR", registers.hl);
				cb_rotate(&tmp, 0);
			}
			write_byte_unsigned(registers.hl, tmp);
			ticks += 8;
			break;
		case 0x28: case 0x29: case 0x2A: case 0x2B: case 0x2C: case 0x2D: case 0x2F:	// SRA r
			printf(msg_format_2_str, pc, "SRA", array_of_reg_letters[SRC_REG_INDEX(opcode)]);
			cb_sra(array_of_regs[SRC_REG_INDEX(opcode)]);
			ticks += 4;
			break;
		case 0x20: case 0x21: case 0x22: case 0x23: case 0x24: case 0x25: case 0x27:	// SLA r
		case 0x38: case 0x39: case 0x3A: case 0x3B: case 0x3C: case 0x3D: case 0x3F:	// SRL r
			if (opcode >= 0x38) {
				printf(msg_format_2_str, pc, "SRL", array_of_reg_letters[SRC_REG_INDEX(opcode)]);
				cb_sla_srl(array_of_regs[SRC_REG_INDEX(opcode)], 0);
			} else {
				printf(msg_format_2_str, pc, "SLA", array_of_reg_letters[SRC_REG_INDEX(opcode)]);
				cb_sla_srl(array_of_regs[SRC_REG_INDEX(opcode)], 1);
			}
			ticks += 4;
			break;
		case 0x26:	// SLA (HL)
		case 0x3E:	// SRL (HL)
			tmp = read_byte_unsigned(registers.hl);
			if (opcode == 0x3E) {
				printf(msg_format_2_hex, pc, "SRL", tmp);
				cb_sla_srl(&tmp, 0);
			} else {
				printf(msg_format_2_hex, pc, "SLA", tmp);
				cb_sla_srl(&tmp, 1);
			}
			write_byte_unsigned(registers.hl, tmp);
			ticks += 8;
			break;
		case 0x2E:	// SRA (HL)
			tmp = read_byte_unsigned(registers.hl);
			printf(msg_format_2_ahex, pc, "SRA", registers.hl);
			cb_sra(&tmp);
			write_byte_unsigned(registers.hl, tmp);
			ticks += 8;
			break;
		case 0x30: case 0x31: case 0x32: case 0x33: case 0x34: case 0x35: case 0x37: // SWAP r
			printf(msg_format_2_str, pc, "SWAP", array_of_reg_letters[SRC_REG_INDEX(opcode)]);
			cb_swap(&registers.a);
			ticks += 4;
			break;
		case 0x36:	// SWAP (HL)
			tmp = read_byte_unsigned(registers.hl);
			printf(msg_format_2_ahex, pc, "SWAP", registers.hl);
			cb_swap(&tmp);
			write_byte_unsigned(registers.hl, tmp);
			ticks += 8;
			break;
	case 0x40: case 0x41: case 0x42: case 0x43: case 0x44: case 0x45: case 0x47:	// BIT 0,r
	case 0x48: case 0x49: case 0x4A: case 0x4B: case 0x4C: case 0x4D: case 0x4F:	// BIT 1,r
	case 0x50: case 0x51: case 0x52: case 0x53: case 0x54: case 0x55: case 0x57:	// BIT 2,r
	case 0x58: case 0x59: case 0x5A: case 0x5B: case 0x5C: case 0x5D: case 0x5F:	// BIT 3,r
	case 0x60: case 0x61: case 0x62: case 0x63: case 0x64: case 0x65: case 0x67:	// BIT 4,r
	case 0x68: case 0x69: case 0x6A: case 0x6B: case 0x6C: case 0x6D: case 0x6F:	// BIT 5,r
	case 0x70: case 0x71: case 0x72: case 0x73: case 0x74: case 0x75: case 0x77:	// BIT 6,r
	case 0x78: case 0x79: case 0x7A: case 0x7B: case 0x7C: case 0x7D: case 0x7F:	// BIT 7,r
		bitpos = CB_BIT_POS(opcode);
		printf(msg_format_3_int_str, pc, "BIT", bitpos, array_of_reg_letters[SRC_REG_INDEX(opcode)]);
		cb_bit(bitpos, *(array_of_regs[SRC_REG_INDEX(opcode)]));
		ticks += 4;
		break;
	case 0x46: case 0x4E:	// BIT 0,(HL);	BIT 1,(HL)
	case 0x56: case 0x5E:	// BIT 2,(HL);	BIT 3,(HL)
	case 0x66: case 0x6E:	// BIT 4,(HL);	BIT 5,(HL)	
	case 0x76: case 0x7E:	// BIT 6,(HL);	BIT 7,(HL)
		tmp = read_byte_unsigned(registers.hl);
		bitpos = CB_BIT_POS(opcode);
		printf(msg_format_3_int_ahex, pc, "BIT", bitpos, tmp);
		cb_bit(bitpos, tmp);
		ticks += 8;
		break;
	case 0x80: case 0x81: case 0x82: case 0x83: case 0x84: case 0x85: case 0x87:	// RES 0,r
	case 0x88: case 0x89: case 0x8A: case 0x8B: case 0x8C: case 0x8D: case 0x8F:	// RES 1,r
	case 0x90: case 0x91: case 0x92: case 0x93: case 0x94: case 0x95: case 0x97:	// RES 2,r
	case 0x98: case 0x99: case 0x9A: case 0x9B: case 0x9C: case 0x9D: case 0x9F:	// RES 3,r
	case 0xA0: case 0xA1: case 0xA2: case 0xA3: case 0xA4: case 0xA5: case 0xA7:	// RES 4,r
	case 0xA8: case 0xA9: case 0xAA: case 0xAB: case 0xAC: case 0xAD: case 0xAF:	// RES 5,r
	case 0xB0: case 0xB1: case 0xB2: case 0xB3: case 0xB4: case 0xB5: case 0xB7:	// RES 6,r
	case 0xB8: case 0xB9: case 0xBA: case 0xBB: case 0xBC: case 0xBD: case 0xBF:	// RES 7,r
		bitpos = CB_BIT_POS(opcode);
		printf(msg_format_3_int_str, pc, "RES", bitpos, array_of_reg_letters[SRC_REG_INDEX(opcode)]);
		cb_res(array_of_regs[SRC_REG_INDEX(opcode)], bitpos);
		ticks += 4;
		break;
	case 0x86: case 0x8E:	// RES 0,(HL);	RES 1,(HL)
	case 0x96: case 0x9E:	// RES 2,(HL);	RES 3,(HL)
	case 0xA6: case 0xAE:	// RES 4,(HL);	RES 5,(HL)
	case 0xB6: case 0xBE:	// RES 6,(HL);	RES 7,(HL)
		tmp = read_byte_unsigned(registers.hl);
		bitpos = CB_BIT_POS(opcode);
		printf(msg_format_3_int_ahex, pc, "RES", bitpos, registers.hl);
		cb_res(&tmp, bitpos);
		write_byte_unsigned(registers.hl, tmp);
		ticks += 8;
		break;
	case 0xC0: case 0xC1: case 0xC2: case 0xC3: case 0xC4: case 0xC5: case 0xC7:	// SET 0,r
	case 0xC8: case 0xC9: case 0xCA: case 0xCB: case 0xCC: case 0xCD: case 0xCF:	// SET 1,r
	case 0xD0: case 0xD1: case 0xD2: case 0xD3: case 0xD4: case 0xD5: case 0xD7:	// SET 2,r
	case 0xD8: case 0xD9: case 0xDA: case 0xDB: case 0xDC: case 0xDD: case 0xDF:	// SET 3,r
	case 0xE0: case 0xE1: case 0xE2: case 0xE3: case 0xE4: case 0xE5: case 0xE7:	// SET 4,r
	case 0xE8: case 0xE9: case 0xEA: case 0xEB: case 0xEC: case 0xED: case 0xEF:	// SET 5,r
	case 0xF0: case 0xF1: case 0xF2: case 0xF3: case 0xF4: case 0xF5: case 0xF7:	// SET 6,r
	case 0xF8: case 0xF9: case 0xFA: case 0xFB: case 0xFC: case 0xFD: case 0xFF:	// SET 7,r
		bitpos = CB_BIT_POS(opcode);
		printf(msg_format_3_int_str, pc, "SET", bitpos, array_of_reg_letters[SRC_REG_INDEX(opcode)]);
		cb_set(array_of_regs[SRC_REG_INDEX(opcode)], bitpos);
		ticks += 4;
		break;
	case 0xC6: case 0xCE:	// SET 0,(HL);	SET 1,(HL)
	case 0xD6: case 0xDE:	// SET 2,(HL);	SET 3,(HL)
	case 0xE6: case 0xEE:	// SET 4,(HL);	SET 5,(HL)
	case 0xF6: case 0xFE:	// SET 6,(HL);	SET 7,(HL)
		bitpos = CB_BIT_POS(opcode);
		tmp = read_byte_unsigned(registers.hl);
		printf(msg_format_3_int_ahex, pc, "SET", bitpos, registers.hl);
		cb_set(&tmp, bitpos);
		write_byte_unsigned(registers.hl, tmp);
		ticks += 8;
		break;
	default:
		printf(msg_format_cb_err, registers.pc, (opcode & 0xFF));
		return 1;
	}
	return 0;
}

/*
 * CB-prefixed opcode implementations.
 */

void cb_bit(uint8_t bit, uint8_t val) {
	registers.half_carry = 1;
	registers.subtract = 0;
	registers.zero = !((val >> bit) & 1);
}

void cb_swap(uint8_t* param) {
	// Swap the lower nibble with the upper
	*param = ((*param & 0x0F) << 4) | ((*param & 0xF0) >> 4);

	if (*param) {
		registers.zero = 0;
	} else {
		registers.zero = 1;
	}

	registers.subtract = 0;
	registers.half_carry = 0;
	registers.carry = 0;
}

void cb_set(uint8_t* param, uint8_t bit) {
	*param |= (1 << bit);
}

void cb_sla_srl(uint8_t* param, uint8_t left) {
	uint8_t tmp = *param;
	if (left) {	// SLA
		registers.carry = !!(tmp & 0x80);
		tmp = tmp << 1;
	} else {	// SRL
		registers.carry = (tmp & 0x1);
		tmp = tmp >> 1;
	}

	registers.half_carry = 0;
	registers.subtract = 0;
	registers.zero = !!tmp;

	*param = tmp;
}

void cb_rotate_carry(uint8_t* param, uint8_t left) {
	uint8_t tmp = *param;

	if (left) {	// RLC
		// Store bit 7 in the carry flag
		registers.carry = !!(tmp & 0x80);
		// Left shift by one
		tmp = tmp << 1;
		// Set bit 0 if the carry flag is set
		if (registers.carry) {
			tmp |= 1;
		} else {
			// Clear it otherwise
			tmp &= ~1;
		}
	} else {	// RRC
		registers.carry = (tmp & 1);
		tmp = tmp >> 1;
		if (registers.carry) {
			tmp |= (1 << 7);
		} else {
			tmp &= ~(1 << 7);
		}
	}

	*param = tmp;
}

void cb_rotate(uint8_t* param, uint8_t left) {
	uint8_t tmp = *param;
	uint8_t tmpCarry = 0;

	if (left) {	// RL
		tmpCarry = !!(tmp & 0x80);
		// Shift left by one
		tmp = tmp << 1;

		// If the carry bit is set, then set bit 0
		if (registers.carry) {
			tmp |= 1;
		} else {
			// Otherwise, clear it
			tmp &= ~1;
		}
	} else {	// RR
		tmpCarry = (tmp & 1);
		// Shift right by one
		tmp = tmp >> 1;

		if (registers.carry) {
			tmp |= (1 << 7);
		} else {
			tmp &= ~(1 << 7);
		}
	}

	registers.carry = tmpCarry;
	*param = tmp;
}

void cb_sra(uint8_t* param) {
	uint8_t tmp = *param;
	
	// Set the carry flag if bit 0 is set
	registers.carry = (tmp & 1);
	// Shift all bits right by one, keeping bit 7
	// at its original value
	tmp = (tmp & 0x80) + (tmp >> 1);

	if (tmp) {
		registers.zero = 0;
	} else {
		registers.zero = 1;
	}

	registers.subtract = 0;

	*param = tmp;
}

void cb_res(uint8_t* param, uint8_t bit) {
	*param &= ~(1 << bit);
}

