/*
 * memory.h
 *
 *  Created on: Dec 12, 2016
 *      Author: fzolp
 */

#ifndef MEMORY_H_
#define MEMORY_H_

#include <inttypes.h>

#define ROM_BANK0_START		0x0000
#define ROM_BANK0_END		0x3FFF
#define ROM_BANKX_START		0x4000
#define ROM_BANKX_END		0x7FFF
#define VRAM_START			0x8000
#define VRAM_END			0x9FFF
#define EXT_RAM_START		0xA000
#define EXT_RAM_STOP		0xBFFF
#define IRAM_START			0xC000
#define IRAM_END			0xCFFF
#define WRAM_BANK_START		0xD000
#define WRAM_BANK_END		0xDFFF
#define IRAM_ECHO_START		0xE000
#define IRAM_ECHO_END		0xFDFF
#define OAM_START			0xFE00
#define OAM_END				0xFE9F
#define IO_START			0xFF00
#define IO_END				0xFF7F
#define HRAM_START			0xFF80
#define HRAM_END			0xFFFE
#define INTERRUPT_ENABLE	0xFFFF

#define VRAM_SIZE		((VRAM_END - VRAM_START) + 1)
#define EXT_RAM_SIZE	((EXT_RAM_END - EXT_RAM_START) + 1)
#define IRAM_SIZE		((IRAM_END - IRAM_START) + 1)
#define WRAM_SIZE		((WRAM_BANK_END - WRAM_BANK_START) + 1)
#define HRAM_SIZE		((HRAM_END - HRAM_START) + 1)
#define OAM_SIZE		((OAM_END - OAM_START) + 1)
#define IO_SIZE			((IO_END - IO_START) + 1)

#define gb_write(a, v)		_Generic ((v),			\
								int8_t: 	write_byte_signed,	\
								uint8_t:	write_byte_unsigned,\
								int16_t:	write_word_signed,	\
								uint16_t:	write_word_unsigned)(a, v)

void write_byte_signed(uint16_t addr, int8_t val);
void write_byte_unsigned(uint16_t addr, uint8_t val);
void write_word_signed(uint16_t addr, int16_t val);
void write_word_unsigned(uint16_t addr, uint16_t val);

uint8_t read_byte_dbg(uint16_t addr);
uint8_t read_byte_unsigned(uint16_t addr);
int8_t read_byte_signed(uint16_t addr);
uint16_t read_word_unsigned(uint16_t addr);
int16_t read_word_signed(uint16_t addr);

void memory_init(void);

#endif /* MEMORY_H_ */
