/*
 * interrupt.h
 *
 *  Created on: Dec 14, 2016
 *      Author: fzolp
 */

#ifndef INTERRUPT_H_
#define INTERRUPT_H_

#include <inttypes.h>

#define OFFSET_IF		0xFF0F

struct interrupt_enable {
	union {
		struct {
			uint8_t vblank_enable : 1;
			uint8_t lcd_stat_enable : 1;
			uint8_t timer_enable : 1;
			uint8_t serial_enable : 1;
			uint8_t joypad_enable : 1;
			uint8_t unused : 3;
		};
		uint8_t int_en;
	};
};

struct interrupt_flag {
	union {
		struct {
			uint8_t vblank_irq : 1;
			uint8_t lcd_stat_irq : 1;
			uint8_t timer_irq : 1;
			uint8_t serial_irq : 1;
			uint8_t joypad_irq : 1;
		};
		uint8_t int_flag;
	};
};

void step_interrupt(void);

void vblank_interrupt(void);
void lcd_stat_interrupt(void);
void timer_interrupt(void);
void joypad_interrupt(void);
void serial_interrupt(void);

void return_interrupt(void);

#endif /* INTERRUPT_H_ */
