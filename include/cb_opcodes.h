#ifndef __CB_OPCODES_H__
#define __CB_OPCODES_H__
#include <inttypes.h>

uint8_t cb_prefix(uint8_t pc, uint8_t opcode);

#endif /* __CB_OPCODES_H__ */
