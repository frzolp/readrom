#ifndef __MBC_H__
#define __MBC_H__

#include <inttypes.h>

void mbc_config(uint16_t addr, uint8_t val);

#endif /* __MBC_H__ */
