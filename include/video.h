/*
 * video.h
 *
 *  Created on: Dec 12, 2016
 *      Author: fzolp
 */

#ifndef VIDEO_H_
#define VIDEO_H_

#include <inttypes.h>

#define OFFSET_LCDC 	0xFF40
#define OFFSET_STAT		0xFF41

#define STAT_HBLANK		0
#define STAT_VBLANK		1
#define STAT_OAM		2
#define STAT_TRANSFER	3

struct lcdc {
	uint8_t bg_display				: 1;
	uint8_t obj_display_en 			: 1;
	uint8_t obj_size				: 1;
	uint8_t bg_tile_map_sel			: 1;
	uint8_t bg_win_tile_data_sel	: 1;
	uint8_t win_display_en			: 1;
	uint8_t win_tile_map_sel		: 1;
	uint8_t lcd_display_en			: 1;
};

struct stat {
	uint8_t mode_flag				: 2;
	uint8_t coincidence				: 1;
	uint8_t mode0_interrupt			: 1;
	uint8_t mode1_interrupt			: 1;
	uint8_t mode2_interrupt			: 1;
	uint8_t coincidence_interrupt 	: 1;
};

struct tile {
	uint8_t row_0[2];
	uint8_t row_1[2];
	uint8_t row_2[2];
	uint8_t row_3[2];
	uint8_t row_4[2];
	uint8_t row_5[2];
	uint8_t row_6[2];
	uint8_t row_7[2];
};

/*
 * Video Status/Control Registers
 *
 * The status and control registers for the display
 * controller are mapped to the memory address space
 * starting at address 0xFF40.
 */
struct video_registers {
	union {
		struct {
			union {
				struct {
					uint8_t bg_display 				: 1;	// Bit 0: BG Display (0=Off, 1=On)
					uint8_t obj_display_enable 		: 1;	// Bit 1: OBJ (Sprite) Display Enable (0=Off, 1=On)
					uint8_t obj_size 				: 1;	// Bit 2: OBJ (Sprite) Size (0=8x8, 1=8x16)
					uint8_t bg_display_select 		: 1;	// Bit 3: BG Tile Map Display Select (0=9800-9Bff, 1=9C00-9FFF)
					uint8_t bg_window_data_select 	: 1;	// Bit 4: BG & Window Tile Data Select (0=8800-97FF, 1=8000-8FFF)
					uint8_t window_display_enable 	: 1;	// Bit 5: Window Display Enable (0=Off, 1=On)
					uint8_t window_display_select 	: 1;	// Bit 6: Window Tile Map Display Select (0=9800-9BFF, 1=9C00-9FFF)
					uint8_t lcd_display_enable 		: 1;	// Bit 7: LCD Display Enable (0=Off, 1=On)
				};
				uint8_t lcdc;					// 0xFF40: LCDC - LCD Control
			};
			union {
				struct {
					uint8_t mode_flag 			: 2;	// Bit 0-1: Mode Flag (0=H-Blank, 1=V-Blank, 2=OAM Search, 3=Tx to Driver) (RO)
					uint8_t coincidence_flag 	: 1;	// Bit 2:   Coincidence Flag (0=LYC!=LY, 1=LYC==LY) (RO)
					uint8_t hblank_interrupt 	: 1;	// Bit 3:   Mode 0 H-Blank Interrupt (0=Disable, 1=Enable) (RW)
					uint8_t vblank_interrupt 	: 1;	// Bit 4:   Mode 1 V-Blank Interrupt (0=Disable, 1=Enable) (RW)
					uint8_t oam_interrupt 		: 1;	// Bit 5:   Mode 2 OAM Interrupt (0=Disable, 1=Enable) (RW)
					uint8_t lyc_interrupt 		: 1;	// Bit 6:   LY Coincidence Interrupt (0=Disable, 1=Enable) (RW)
					uint8_t ignored 			: 1;	// Bit 7:   IGNORED
				};
				uint8_t stat;					// 0xFF41: STAT - LCD Status
			};
			uint8_t scy;						// 0xFF42: SCY - Scroll Y (RW)
			uint8_t scx;						// 0xFF43: SCX - Scroll X (RW)
			uint8_t ly;							// 0xFF44: LY - LCDC Y-Coordinate (RO)
			uint8_t lyc;						// 0xFF45: LYC - LY Compare (RW)
			uint8_t dma;						// 0xFF46: DMA - DMA Transfer and Start Address (RW)
			union {
				struct {
					uint8_t color_0 : 2;			// Bit 0-1: Shade for color 0
					uint8_t color_1 : 2;			// Bit 2-3: Shade for color 1
					uint8_t color_2 : 2;			// Bit 4-5: Shade for color 2
					uint8_t color_3 : 2;			// Bit 6-7: Shade for color 3
				};
				uint8_t bgp;					// 0xFF47: BG Palette Data (RW)
			};
			uint8_t obp_0;						// 0xFF48: OBP0 - Object Palette 0 Data (RW)
			uint8_t obp_1;						// 0xFF49: OBP1 - Object Palette 1 Data (RW)
			uint8_t wy;							// 0xFF4A: WY - Window Y Position (RW)
			uint8_t wx;							// 0xFF4B: WX - Window X Position (RW)
		};
		uint8_t video_registers[12];
	};
};

struct oam {
	uint8_t y_pos;
	uint8_t x_pos;
	uint8_t tile_num;
	union {
		struct {
			uint8_t color_palette_num : 3;
			uint8_t color_tile_bank : 1;
			uint8_t palette_num : 1;
			uint8_t x_flip : 1;
			uint8_t y_flip : 1;
			uint8_t obj_priority : 1;
		};
		uint8_t attributes;
	};
};

void lcd_init(void);
void step_lcd(void);

#endif /* VIDEO_H_ */
