#ifndef __CPU_OPCODES_H__
#define __CPU_OPCODES_H__

#include <inttypes.h>

/*
 * 8-bit Load ops
 * - LD r,r' 	- reg, reg  - Load from register into register
 * - LD r,n	 	- reg, byte - Load immediate data into register
 * - LD r,(RR)	- reg, ptr  - Load byte from address (RR) into register
 * - LD (RR),r	- ptr, reg  - Load from register into address (RR)
 * - LD (RR),n	- ptr, byte - Load immediate data into address (RR)
 * - LD r,(r)	- reg, ptr  - Load byte from address (r)+0xFF00 into register
 * - LD (r),r	- ptr, reg  - Load from register into address (r)+0xFF00
 * - LD r,(nn)	- reg, ptr  - Load byte from address (nn) into register
 * - LD A,(HLI)	- reg, ptr  - Load byte from address (HL) into register A, increment HL
 * - LD A,(HLD)	- reg, ptr  - Load byte from address (HL) into register A, decrement HL
 * - LD (HLI),A	- ptr, reg  - Load register A into address (HL), increment HL
 * - LD (HLD),A	- ptr, reg  - Load register A into address (HL), decrement HL
 */

/*
 * 16-bit Load ops
 * - LD RR,nn	 - reg, word	- Load immediate data into register pair
 * - LD (nn),RR	 - ptr, reg 	- Load register pair into address (nn)
 * - LD RR,RR'	 - reg, reg 	- Load register pair into register pair
 * - LD RR,RR'+n - reg, reg+byte- Load register pair +/- n into register pair
 */

void daa(void);
void rotate(uint8_t* param, uint8_t left, uint8_t carry);
void byte_add(uint8_t* dest, uint8_t value);
void word_add(uint16_t* dest, uint16_t value);
void byte_sub(uint8_t* dest, uint8_t value);

// Boolean ops
void byte_dec(uint8_t* dest);
void byte_and(uint8_t param);
void byte_xor(uint8_t param);
void byte_or(uint8_t param);
void byte_cp(uint8_t param);

// Stack ops
void stack_push(uint16_t val);
uint16_t stack_pop(void);

#endif /* __CPU_OPCODES_H__ */
