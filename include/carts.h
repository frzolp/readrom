#ifndef __CARTS_H__
#define __CARTS_H__

#include <inttypes.h>

typedef struct {
	uint8_t nop[0x104];
	uint8_t nintendo[0x30];
	uint8_t title[0x0F];
	uint8_t color;
	uint8_t hi_license;
	uint8_t lo_license;
	uint8_t super_gb;
	uint8_t cart_type;
	uint8_t rom_size;
	uint8_t ram_size;
	uint8_t destination;
	uint8_t licensee_code;
	uint8_t mask_rom_version;
	uint8_t complement_check;
	uint8_t checksum[0x02];
} gb_rom;

#endif /* __CARTS_H__ */
