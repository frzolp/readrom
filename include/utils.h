#ifndef __UTILS_H__
#define __UTILS_H__

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_RED_BOLD	"\x1b[31;1m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_CYAN_BOLD	"\x1b[36;1m"
#define ANSI_COLOR_RESET   "\x1b[0m"

void populate_romtypes(void);
void print_regs(void);
void parse_rom(void);
int nintendo_verify(void);
void cart_details(void);
void dump_ram(void);
void print_io_registers(void);
void print_logo(void);

#endif /* __UTILS_H__ */
