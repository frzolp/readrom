#ifndef __REGISTERS_H__
#define __REGISTERS_H__

#include <inttypes.h>

/*
 * CPU Registers structure
 * 
 * The Game Boy uses an 8-bit CPU related to Intel's 8080 and
 * Zilog's Z80 processors. This struct represents the CPU's
 * registers in 8-bit and 16-bit forms. The use of anonymous
 * structs and unions requires C11 mode.
 *
 * THE LAYOUT
 *
 * 15-----8-----0
 *   |  A |  F |
 *   -----------
 *   |  B |  C |
 *   -----------
 *   |  D |  E |
 *   -----------
 *   |  H |  L |
 *   -----------
 *
 */
struct registers {
	/*
	 * A and F registers
	 *
	 * The F, or Flag register, makes this initial structure look
	 * complicated, but it makes code cleaner overall since this
	 * won't be running on a bunch of different architectures.
	 *
	 * Layout of the F register:
	 *     7    6    5    4    3    2    1    0
	 *  | ZF |  N |  H | CY |       Unused      |
	 *
	 * I want to access the fields directly without bit-shifting, so
	 * F is represented as a union between (uint8_t f) and an anonymous
	 * bitfield. This means we can get/set the register flags directly
	 * and avoid shifting and masking.
	 */
	union {
		/* 8-bit representation of A and F */
		struct {
			/* Represent F as both an 8-bit register and a bitfield */
			union {
				struct {
					uint8_t unused 		: 4;	// Bits 0-3 unused
					uint8_t carry 		: 1;	// Bit 4: Carry
					uint8_t half_carry 	: 1;	// Bit 5: Half Carry
					uint8_t subtract 	: 1;	// Bit 6: Subtract
					uint8_t zero 		: 1;	// Bit 7: Zero
				};
				uint8_t f;	// 8-bit F register
			};
			uint8_t a;		// 8-bit A register
		};
		uint16_t af;		// 16-bit AF register
	};
	/* B and C registers */
	union {
		struct {
			uint8_t c;		// 8-bit C register
			uint8_t b;		// 8-bit B register
		};
		uint16_t bc;		// 16-bit BC register
	};
	/* D and E registers */
	union {
		struct {
			uint8_t e;		// 8-bit E register
			uint8_t d;		// 8-bit D register
		};
		uint16_t de;		// 16-bit DE register
	};
	/* H and L registers */
	union {
		struct {
			uint8_t l;		// 8-bit L register
			uint8_t h;		// 8-bit H register
		};
		uint16_t hl;		// 16-bit HL register
	};
	uint16_t sp;			// 16-bit stack pointer
	uint16_t pc;			// 16-bit program counter
};
#endif /* __REGISTERS_H__ */
