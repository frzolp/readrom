#ifndef __CPU_H__
#define __CPU_H__

#include <inttypes.h>

/*
 * Register values as they pertain to opcodes.
 * For example, register XOR opcodes are
 * 0b10101nnn with the last three bits indicating
 * the register to operate on.
 * "XOR A" = 0b10101 111 = 0xAF
 * "XOR C" = 0b10101 001 = 0xA9
 *
 * "LD B,B" = 0b01 000 000 = 0x40
 * "LD B,C" = 0b01 000 001 = 0x41
 *
 * Taken from GameBoy Programming Manual v1.1, 
 * Chapter 4: "CPU Instruction Set", page 95.
 *
 * NOTE: Register D is wrong in the document.
 * The correct values are listed here.
 * 		___________________
 * 		| Reg | Bin | Hex |
 * 		|=====|=====|=====|
 * 		|  A  | 111 | 0x7 |
 * 		|-----|-----|-----|
 * 		|  B  | 000 | 0x0 |
 * 		|-----|-----|-----|
 * 		|  C  | 001 | 0x1 |
 * 		|-----|-----|-----|
 * 		|  D  | 010 | 0x2 |
 * 		|-----|-----|-----|
 * 		|  E  | 011 | 0x3 |
 * 		|-----|-----|-----|
 * 		|  H  | 100 | 0x4 |
 * 		|-----|-----|-----|
 * 		|  L  | 101 | 0x5 |
 * 		|_____|_____|_____|
 */
#define REG_A			0x7		/* 0b111 */
#define REG_B			0x0		/* 0b000 */
#define REG_C			0x1		/* 0b001 */
#define REG_D			0x2		/* 0b010 */
#define REG_E			0x3		/* 0b011 */
#define REG_H			0x4		/* 0b100 */
#define REG_L			0x5		/* 0b101 */
/* 0x6 is reserved for the (HL) operand */

/*
 * Register pair values pertaining to opcodes.
 * Same logic as above.
 *
 * NOTE: GameBoy Programming Manual 1.1 mistakenly
 * lists register pair DE as DD.
 */
#define REGPAIR_BC		0x0		/* 0b00 */
#define REGPAIR_DE		0x1		/* 0b01 */
#define REGPAIR_HL		0x2		/* 0b10 */
#define REGPAIR_SP		0x3		/* 0b11 */

/*
 * Some macros to handle checking, setting,
 * and clearing of CPU flags.
 */
#define SET_ZERO(r) 	r.f |= (1 << 7)
#define CLR_ZERO(r) 	r.f &= ~(1 << 7)
#define IS_ZERO(r)	!!(r.f & (1 << 7))
#define SET_SUB(r) 	r.f |= (1 << 6)
#define CLR_SUB(r) 	r.f &= ~(1 << 6)
#define IS_SUB(r)	!!(r.f & (1 << 6))
#define SET_HCARRY(r) 	r.f |= (1 << 5)
#define CLR_HCARRY(r) 	r.f &= ~(1 << 5)
#define IS_HCARRY(r)	!!(r.f & (1 << 5))
#define SET_CARRY(r) 	r.f |= (1 << 4)
#define CLR_CARRY(r) 	r.f &= ~(1 << 4)
#define IS_CARRY(r)	!!(r.f & (1 << 4))

#define FLAG_ZERO	7
#define FLAG_NEGATIVE	6
#define FLAG_HALFCARRY	5
#define FLAG_CARRY	4

#define SET_FLAG(r,l)	r.f |= (1 << l)
#define CLR_FLAG(r,l)	r.f &= ~(1 << l)

/*
 * Macros to check if an operation performed a half carry
 */
#define HALF_CARRY_CHECK_8(d, v)   ((d ^ v ^ (d + v)) & 0x10)
#define HALF_CARRY_CHECK_16(d, v)  ((d ^ v ^ (d + v)) & 0x1000)

/*
 * Macros to map 8-bit and 16-bit add/sub functions to the
 * appropriate implementation based on data type of the
 * parameter.
 */
#define add(x, y)	_Generic((x),	\
	uint8_t: byte_add,		\
	uint16_t: word_add)(x, y)

#define sub(x, y) _Generic((x), \
  uint8_t: byte_sub,  \
  uint16_t: word_sub)(x, y)

void byte_add(uint8_t* dest, uint8_t value);
void word_add(uint16_t* dest, uint16_t value);
void byte_sub(uint8_t* dest, uint8_t value);
void word_sub(uint16_t* dest, uint16_t value);
void byte_dec(uint8_t* dest);
void byte_xor(uint8_t dest);
void byte_or(uint8_t param);
void byte_and(uint8_t param);

void byte_cp(uint8_t param);

void stack_push(uint16_t val);
uint16_t stack_pop(void);

uint8_t cb_prefix(uint8_t pc, uint8_t opcode);
void cb_bit(uint8_t bit, uint8_t val);

void cpu_init(void);
uint8_t cpu_step(void);

#endif /* __CPU_H__ */
