#ifndef __DISPLAY_H__
#define __DISPLAY_H__

#include <inttypes.h>

typedef struct {
	uint8_t tile[32];
} bg_tile_map_row;

typedef struct {
	uint8_t row0[2];
	uint8_t row1[2];
	uint8_t row2[2];
	uint8_t row3[2];
	uint8_t row4[2];
	uint8_t row5[2];
	uint8_t row6[2];
	uint8_t row7[2];
} tile_data;

typedef union {
	tile_data tiles[32];
	uint8_t bytes[sizeof(tile_data) * 32];
} tile_data_table;

typedef struct {
	uint8_t y_pos;		// Y Position (minus 16)
	uint8_t x_pos;		// X Position (minus 8)
	uint8_t tile_num;	// Tile/Pattern Number (8x16: upper tile (num & 0xFE), lower tile (num | 0x01))
	union {
		struct {
			uint8_t color_palette_num 	: 3;	// (CGB ONLY) Palette Number
			uint8_t color_tile_bank 	: 1;	// (CGB ONLY) Tile VRAM Bank
			uint8_t palette_num 		: 1;	// Palette Number (0=OBP0, 1=OBP1)
			uint8_t x_flip 			: 1;	// X Flip (0=Normal, 1=Horizontal mirrored)
			uint8_t y_flip 			: 1;	// Y Flip (0=Normal, 1=Vertical mirrored)
			uint8_t priority 		: 1;	// OBJ-to-BG Priority (0=Above BG, 1=Behind BG)
		};
		uint8_t attributes;
	};
} object_attribute;

typedef struct {
	object_attribute object[40];
} object_attribute_memory;

#endif /* __DISPLAY_H__ */
