/*
 * audio.h
 *
 *  Created on: Dec 13, 2016
 *      Author: fzolp
 */

#ifndef AUDIO_H_
#define AUDIO_H_

#include <inttypes.h>

struct audio_registers {
	union {
		struct {
			uint8_t sweep_shift_num : 3;
			uint8_t sweep_inc_dec : 1;
			uint8_t sweep_time : 3;
			uint8_t nr10_unused : 1;
		};
		uint8_t nr10;
	};
	union {
		struct {
			uint8_t m1_sound_length : 6;
			uint8_t m1_wave_pattern_duty : 2;
		};
		uint8_t nr11;
	};
	union {
		struct {
			uint8_t m1_envelope_sweep : 3;
			uint8_t m1_envelope_up_dn : 1;
			uint8_t m1_envelope_vol : 4;
		};
		uint8_t nr12;
	};
	union {
		struct {
			uint16_t m1_frequency: 11;
			uint16_t nr14_unused : 3;
			uint16_t m1_ctr_cons_select : 1;
			uint16_t m1_initial : 1;
		};
		struct {
			uint8_t nr13;
			uint8_t nr14;
		};
		uint16_t nr13_14;
	};
	uint8_t ignored;
	union {
		struct {
			uint8_t m2_sound_length : 6;
			uint8_t m2_wave_pattern_duty : 2;
		};
		uint8_t nr21;
	};
	union {
		struct {
			uint8_t m2_envelope_sweep : 3;
			uint8_t m2_envelope_up_dn : 1;
			uint8_t m2_envelope_vol : 4;
		};
		uint8_t nr22;
	};
	union {
		struct {
			uint16_t m2_frequency : 11;
			uint16_t nr19_unused : 3;
			uint16_t m2_ctr_cons_select : 1;
			uint16_t m2_initial : 1;
		};
		struct {
			uint8_t nr23;
			uint8_t nr24;
		};
		uint16_t nr23_24;
	};
	union {
		struct {
			uint8_t m3_ignored: 7;
			uint8_t m3_sound_off : 1;
		};
		uint8_t nr30;
	};
	// TODO: NR31+
};

#endif /* AUDIO_H_ */
